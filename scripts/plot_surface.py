from mpl_toolkits import mplot3d
# plotting
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.tools as tls
tls.set_credentials_file(username='mo.schulze@tu-braunschweig.de', api_key='ZiAi4QATeoMOI9zEA12M')
import matplotlib.pyplot as plt

import apprentice
import numpy as np

# first run pyoo-app, but stop after OO.setNstart(...)

x_inner_opt = [2.530342026028501, 2.9256155653425555, 0.5139464]
x_outer_opt = [0.9948149553369916, 0.005185044663008398] # there is a third weight, which is 1 - the other two weights


# read starting value of weights (might be weights with which the data was created)
w = np.array([float(v) for v in apprentice.weights.read_pointmatchers(opts.WFILE).values()])
w = w/sum(w) # scaling (sum should be equal to 1)


# run optimization
res = OO._io_eval(w) # should give the same results as x_inner_opt above



x_inner_opt = res[1]


def F(x,y):
    return OO.objective([x,y])

# parameter bounds from approximation file (same for all bins!), which is given to the rational approximation objects
p_L = OO._RA[0]._scaler._Xmin
p_U = OO._RA[0]._scaler._Xmax


x = np.linspace(p_L[0], p_U[0], 50)
y = np.linspace(p_L[1], p_U[1], 50)
# close vicinity
e = 0.000001
x = np.linspace((1-e)*x_inner_opt[0], (1+e)*x_inner_opt[0], 30)
y = np.linspace((1-e)*x_inner_opt[1], (1+e)*x_inner_opt[1], 30)


X, Y = np.meshgrid(x, y)
Z = np.zeros(X.shape)

for (i,x,y) in zip(range(X.size),np.nditer(X),np.nditer(Y)):
    Z[np.unravel_index(i, Z.shape)] = F(x,y)

#Z = F(X,Y) # does not always seem to work because of broadcasting step




fig = plt.figure()
ax = plt.axes(projection='3d')
ax.set_xlabel(OO.pnames[0])
ax.set_ylabel(OO.pnames[1])
ax.set_title("{}".format(x_inner_opt))


# plotly
py.plot([trace1], filename="IO_pseudodata")
plotly_fig = tls.mpl_to_plotly(fig)
trace1 = go.Surface(z=Z, colorscale='Viridis')

# matplotlib
ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                cmap='viridis', edgecolor='none')
fig.savefig('IO_pseudodata_zoom2.png')


