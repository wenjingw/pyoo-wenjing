import pyoo
import os
import subprocess
import numpy as np
import apprentice as app

def create_data(case = 'sip33_3gw1bwout2p_constnoise'):
    # This case study includes four observables (as the approximation file we are using has 4 observables), three of them are good, whereas among them two have the same error variances, and one is bad, i.e. it has a different true parameter vector

    approx_fp = "approximation.json"

    n_p = 2 # number of parameters
    n_o = 4 # number of observable

    # true parameter values with which the approximations were derived
    p_names = ["pT0Ref", "expPow"]
    pT0Ref = 2.4743870853827445
    expPow = 1.7068479855005454

    # we therefore choose the true parameter value for the case study as...
    p0 = np.array([2.47, 1.71])
    # and the parameter value for the bad dataset
    p1 = np.array([3.0, 1.2])

    # finding max value of models
    binids, RA = app.tools.readApprox(approx_fp)
    fmaxs = np.array([r.fmax() for r in RA])
    FMAX = np.max(fmaxs)

    # model_bias = [0.,0.,0.,FMAX+eps_cases[3]]

    # case = 'sip33_3gw1bwout2p_constnoise'
    if case == 'sip33_3gw1bwout2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005,0.005,0.005,0.005])
        model_bias = [0., 0., 0., FMAX + eps_cases[3]]
    elif case == 'sip33_3gw1bwout2p_nonoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.zeros(4)
        model_bias = [0., 0., 0., FMAX + eps_cases[3]]
    elif case == 'sip33_4gw2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005,0.005,0.005,0.005])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_nonoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.zeros(4)
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_3wp0_1wp1_constnoise':
        p_cases = [p0, p0, p0, p1]
        eps_cases = np.array([0.005,0.005,0.005,0.005])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_3wp0_1wp1_nonoise':
        p_cases = [p0, p0, p0, p1]
        eps_cases = np.zeros(4)
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_2gw2bwout2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005,0.005,0.005,0.005])
        model_bias = [0., 0., FMAX + eps_cases[2], FMAX + eps_cases[3]]
    elif case == 'sip33_1gw1bwout1gw1bwout2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.005])
        model_bias = [0., FMAX + eps_cases[1], 0., FMAX + eps_cases[3]]
    elif case == 'sip33_1gw1bwout2gw2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.005])
        model_bias = [0., FMAX + eps_cases[1], 0., 0.]
    elif case == 'sip33_3gw1bwhsd2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.01])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_2gw2bwhsd2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.005, 0.01, 0.01])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_1gw1bwhsd1gw1bwhsd2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.01, 0.005, 0.01])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_1gw1bwhsd2gw2p_constnoise':
        p_cases = [p0, p0, p0, p0]
        eps_cases = np.array([0.005, 0.01, 0.005, 0.005])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_2wp0_2wp1_constnoise':
        p_cases = [p0, p0, p1, p1]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.005])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_1wp0_1wp1_1wp0_1wp1_constnoise':
        p_cases = [p0, p1, p0, p1]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.005])
        model_bias = [0., 0., 0., 0.]
    elif case == 'sip33_4gw2p_1wp0_1wp1_2wp0_constnoise':
        p_cases = [p0, p1, p0, p0]
        eps_cases = np.array([0.005, 0.005, 0.005, 0.005])
        model_bias = [0., 0., 0., 0.]
    else: raise Exception("case: {c} no defined".format(c=case))

    # p_cases = [p0,p0,p0,p0] # parameter vectors for the four observables

    # error variances (more precise error variance factors, see code for more info)
    #eps_cases = [0.005, 0.005, 0.005, 0.005]
    # eps_cases = np.ones(4)


    # outfile path strings
    dir = "../data/{c}".format(c=case)
    if not os.path.exists(dir):
        os.makedirs(dir, exist_ok=True)
    out = "{d}/experimental_data.json".format(d=dir)
    eval = app.tools.artificial_data_from_RA(approx_fp,p_cases,var=eps_cases**2,model_bias=model_bias, outfile=out)
    oute = "{d}/distr_data.json".format(d=dir)
    eval_dict = {'eval':eval,'sd':eps_cases.tolist()}
    import json
    with open(oute,'w') as f:
        json.dump(eval_dict,f)
    from shutil import copyfile
    copyfile(approx_fp, dir+"/approximation.json")
    copyfile('weights', dir + "/weights")

if __name__ == '__main__':

    cases = [
        'sip33_3gw1bwout2p_constnoise',
        'sip33_3gw1bwout2p_nonoise',
        'sip33_4gw2p_constnoise',
        'sip33_4gw2p_nonoise',
        'sip33_4gw2p_3wp0_1wp1_constnoise',
        'sip33_4gw2p_3wp0_1wp1_nonoise',

        'sip33_2gw2bwout2p_constnoise',
        'sip33_1gw1bwout1gw1bwout2p_constnoise',
        'sip33_1gw1bwout2gw2p_constnoise',

        'sip33_3gw1bwhsd2p_constnoise',
        'sip33_2gw2bwhsd2p_constnoise',
        'sip33_1gw1bwhsd1gw1bwhsd2p_constnoise',
        'sip33_1gw1bwhsd2gw2p_constnoise',

        'sip33_4gw2p_2wp0_2wp1_constnoise',
        'sip33_4gw2p_1wp0_1wp1_1wp0_1wp1_constnoise',
        'sip33_4gw2p_1wp0_1wp1_2wp0_constnoise'



    ]
    for c in cases:
        create_data(case=c)
