# run pyoo-app first!
import numpy as np
import findiff as fd
import autograd as ag
import pyoo.outer_objective as oo
from pyoo.plot import *

# plotting
import plotly.plotly as py
import plotly.tools as tls
tls.set_credentials_file(username='mo.schulze@tu-braunschweig.de', api_key='ZiAi4QATeoMOI9zEA12M')
import matplotlib.pyplot as plt

xmin = np.array(RR["X_inner"][-1]) # minimum point inner optimization
wmin = np.array(RR["X_outer"][-1]) # minimum point outer optimization
wmin = np.append(wmin, 1-sum(wmin))
dim = len(xmin)
h = 0.001
supports = []
for i in range(dim):
        supports.append([xmin[i]+d*h*xmin[i] for d in [-2.,-1.,0.,1.,2.]])

X = np.meshgrid(*supports, indexing='ij')

s = X[0].shape
f = np.zeros(s)
for i in range(f.size):
    I = np.unravel_index(i,s)
    p = [X[k][I] for k in range(dim)]
    f[I] = OO.objective(p)

dx = [supports[k][1]-supports[k][0] for k in range(dim)]

HO = {} # Hessian operators
for i in range(dim):
    for j in range(i,dim):
        if i == j:
            HO["%s%s" %(i,j)] = fd.FinDiff((i,dx[i],2))
        else:
            HO["%s%s" %(i, j)] = fd.FinDiff((i,dx[i]), (j,dx[j]))

H = {} # Hessian values
for key, value in HO.items():
    H[key] = value(f)

point_of_interest = tuple(2*np.ones(dim, dtype=int)) # index!, middle point
H_of_interest = np.zeros((dim,dim))
for i in range(dim):
    for j in range(i,dim):
        if i == j:
            H_of_interest[i,i] = H["%s%s" % (i, j)][point_of_interest]
        else:
            H_of_interest[i, j] = H["%s%s" % (i, j)][point_of_interest]
            H_of_interest[j, i] = H["%s%s" % (i, j)][point_of_interest]
H1 = H_of_interest

H0 = RR["H"][-1] # Hessian from the optimizer


# Hessian calculated using autograd and symbolic models
weights = np.sqrt(np.array(OO._W2))
sigma2 = np.array(OO._E2) # measurement error variances
ydata = np.array(OO._Y)
def chi2(x): # objective function
    ymod = np.array([f.eval_f(*x) for f in OO._RA_sym]) # model answers
    #ymod = np.array([f(x) for f in OO._RA])  # model answers
    return sum(weights * (ymod-ydata)**2 * sigma2)

h2 = ag.hessian(chi2)
H2 = h2(xmin) # Hessian at point of interest.

# hessian from objective function
h3 = ag.hessian(OO.objective)
H3 = h3(xmin)

# hessian from the definition of the FIM
# FIM = sum_i w_i [dy/dx]^T C^-1 [dy/dx]
grad_AD = [ag.elementwise_grad(x.eval_f, tuple(range(dim))) for x in OO._RA_sym]
SM = [np.array(g(*xmin))[np.newaxis,:] for g in grad_AD]
FIM = np.zeros((dim,dim))
for i in range(len(SM)):
    FIM += np.sqrt(OO._W2[i])*OO._E2[i]*np.matmul(np.matrix.transpose(SM[i]), SM[i])

# hessian from FIM using sympy gradients
SM2 = [np.array(x.eval_grad(*xmin))[np.newaxis,:] for x in OO._RA_sym]
FIM2 = np.zeros((dim,dim))
for i in range(len(SM2)):
    FIM2 += np.sqrt(OO._W2[i])*OO._E2[i]*np.matmul(np.matrix.transpose(SM2[i]), SM2[i])

FIM3 = sum([oo.fim(SM2[i], np.array(1/OO._E2[i]), np.sqrt(OO._W2[i])) for i in range(len(SM2))])

FIM5 = sum([oo.fim(SM2[i], np.array(1/OO._E2[i])) for i in range(len(SM2))])


FIM4 = OO._WFIM(wmin, *xmin)
FIM4b = OO._WFIM(np.sqrt(OO._W2), *xmin)

FIM0 = RR["WFIM"][-1]



## PLOTTING
## ========

# heatmaps
# --------
matrices = [H0/2/FIM0, H3/2/FIM0, FIM0/FIM0] # divide hessians by 2 as 2 was dropped from likelihood, see doc file for more info
titles = ["Normalized Hessian BFGS update", "Exact normalized Hessian of objective", "Normalized FIM"]
filenames = ["H_BFGS_n", "H_exact_n", "FIM_n"]
# limits of plot
zmin = min([x.min() for x in matrices])
zmax = max([x.max() for x in matrices])


for (M,T,F) in zip(matrices, titles, filenames):
    fig = plt.figure()
    ax = fig.add_subplot(111)

    ax.set_title(T)

    plotly_fig = tls.mpl_to_plotly(fig)

    trace = dict(z=M, type="heatmap", zmin=zmin, zmax=zmax)
    # trace = dict(z=H0, type="heatmap")

    plotly_fig.add_traces([trace])

    plotly_fig['layout']['xaxis'].update({'autorange': True})
    plotly_fig['layout']['yaxis'].update({'autorange': True})

    py.plot(plotly_fig, filename=F)


# confidence ellipsoids
# ---------------------
fig = plt.figure()
ax = fig.add_subplot(111)
confidence_ellipse(0.,0.,cov,ax)