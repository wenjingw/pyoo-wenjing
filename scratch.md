Hello all,

recapping our phone meeting last Friday, it would be beneficial to record a couple of facts in written form to smooth current unclarity and to avoid further ambiguities. In the following we will also describe the approaches we have been developing and what problems we have come or we think we will come across in the future.

What we have done so far:
Basic idea of our approach was to use classical DoE techniques to robustify the parameter estimate.
That means, while the inner optimization is trying to fit the data, the outer optimization tries to minimize some criterion on the uncertainty of the parameter estimate expressed by the parameter covariance matrix.
To calculate the parameter covariance matrix, we need some information on the parameter sensitivities of our model(s).
Here, the classical approach in DoE is to use the Fisher information matrix (FIM), where the inverse gives us a lower bound on the parameter covariance matrix.
We came up with different approximations to the unknown FIM:
1. Hessian
1a. From BFGS update.
1b. Symbolically calculate from objective definition.
2. FIM at parameter estimate from symbolically calculated parameter sensitivities.

We compared the different possibilities and found out that the BFGS hessian is way off the FIM and the exact hessian still differs from the FIM (see report on overleaf).

With the symbolically defined weighted FIM, Mohan was able to introduce gradient-based optimization (interior-point algorithm) in contrast to the RBF approach to the outer optimization.

We have run several tests comparing different DoE criteria and optimization strategies, where we also created validation data to show the functionality of the implemented code.
 We also checked theoretical and practical parameter identifiability (see also report on overleaf).
There is as well a portfolio objective implemented by Julie that does not consider parameter sensitivities, but calculates mean and variances of the predicted errors.

In the following, a couple of comments and questions (the latter mainly to Holger, see point 5):
1. The difference in DoE and in our study case at hand is that we do not design experiments, but use DoE techniques to robustify the parameter estimate.
2. Oscillatory behaviour of polynomials and rationals (the latter are more smooth) might cause multiple local minima in the inner objective.
3. The way the inner objective is implemented is related to finding the maximum likelihood estimator assuming additive uncorrelated Gaussian noise with zero mean (= Chi squared). On the same assumption, the FIM is calculated.
4. For the assumptions in comment 3 and its implementation, the optimization gives us expected results.
5. The important questions left are: First, are the assumptions in comment 3 correct and second, does the objective criterion give us the results we want. Therefore, clarification of the following questions is needed:
5a. Can all the considered observables be modelled by the approach considered in comment 3, i.e. data = model + gaussian error? In other words, are all the observables created from the same parameters and same physical phenomena?
5b. If yes, why is there a pre-filtering of observables? Or what does bad observable mean? Are there different measurement errors for different observables? In the case of FIM, also observables with high uncertainty can improve the parameter estimate.
5c. Or on the other hand, is it possible that the model used is only valid for certain observables and might be a bad choice for other observables?
5d. Another source of error: Might there be a model error coming from the approximations, i.e. the approximations differ from the Monte carlo simulations? And last, are the Monte Carlo simulations maybe not able to catch all the observables at hand?

All in all, it would be nice to get a feeling from where the uncertainties involved come from. That makes modelling them way easier.
