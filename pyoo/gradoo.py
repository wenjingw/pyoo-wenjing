#! /usr/bin/env python
import numpy as np
import sys
from pyoo.outer_objective import OuterObjective

class GOptCaller():


    def __init__(self, *args,
                 OO=None,
                 OOSOLVER='ipopt', OOGRAD='ad',nu=-1., OOJAC = 'ad',
                 mu = 1.,wsp=None, wspc=None, LOGFP='gradopt',starttime=0.,
                 **kwargs):
        self.OO = OO
        self.nparam = self.OO._SCLR.dim
        self.nweight = len(self.OO._hdict)
        self.nbin = len(self.OO._binids)
        self.mu = mu
        self.nu = nu
        self.solveropt = OOSOLVER
        self.logfp = LOGFP
        self.wstartpoint = wsp
        self.wstartpointcomment = wspc
        self.run = self.rungradopt
        self.GBO = GradientBasedOpt(GOC=self)
        self.starttime = starttime
        if OOGRAD == 'ad':
            self.GBO.gradient = self.GBO.gradientAD
        elif OOGRAD == 'fd':
            self.GBO.gradient = self.GBO.gradientFD
        elif OOGRAD == 'analytic':
            self.GBO.gradient = self.GBO.gradientANA
        else:
            raise Exception('{grad} grad type not supported'.format(grad=OOGRAD))

        if OOJAC == 'ad':
            self.GBO.jacobian = self.GBO.jacobianAD
        elif OOJAC == 'analytic':
            if self.nu > 0 and self.OO._metric == 'Egap':
                print("Analytic gradient not available for Egap constraints. Please use AD (option: ad) instead.")
                sys.exit(1)
            self.GBO.jacobian = self.GBO.jacobianANA
        else:
            raise Exception('{jac} jacobian type not supported'.format(jac=OOJAC))


    def rungradopt(self):
        if self.wstartpoint is None:
            np.random.seed(23456744)
            x0 = np.random.uniform(low=0., high=1., size=(self.nweight - 1,))
            x0 = np.append(x0, 1 - np.sum(x0))
        else:
            x0arr = self.wstartpoint.split(',')
            x0 = np.array([float(x) for x in x0arr])

        startpointstr = '_'.join(map(str, x0))

        if self.solveropt == 'cobyla':
            # COBYLA run
            if self.mu == 0:
                cons = [{'type': 'ineq', 'fun': lambda x: sum(x) - (1 - 10 ** -8)},
                        {'type': 'ineq', 'fun': lambda x: -sum(x) + (1 + 10 ** -8)}]
            else: raise Exception("{sol} does not support upper bounds on weights with {b}*\\sigma_b"
                                  .format(sol=self.solveropt,b=self.mu))

            lb = np.zeros(self.nweight, dtype=np.float)
            ub = np.ones(self.nweight, dtype=np.float)

            for i in range(self.nweight):
                cons.append({'type': 'ineq', 'fun': (lambda x, i: x[i] - lb[i]), 'args': (i,)})
                cons.append({'type': 'ineq', 'fun': (lambda x, i: ub[i] - x[i]), 'args': (i,)})

            from scipy.optimize import minimize
            res = minimize(
                self.GBO.objective,
                x0,
                method='COBYLA',
                constraints=cons
            )
            print(res)
            ret = {
                'xoutermin': res['x'],
                'youtermin': res['fun'],
                'status': repr(res['status']),
                'status_msg': repr(res['message']),
                'startpointstr':startpointstr,
                'x0':x0.tolist()
            }
            return ret

        elif self.solveropt == 'slsqp':
            # SLSQP run
            if self.mu == 0:
                cons = [{'type': 'eq', 'fun': lambda x: sum(x) - 1}]
            else: raise Exception("{sol} does not support upper bounds on weights with {b}*\\sigma_b"
                                  .format(sol=self.solveropt,b=self.mu))
            lb = np.zeros(self.nweight, dtype=np.float)
            ub = np.ones(self.nweight, dtype=np.float)
            from scipy.optimize import minimize
            res = minimize(
                self.GBO.objective,
                x0,
                bounds=np.vstack((lb, ub)).T,
                method='SLSQP',
                jac=self.GBO.gradient,
                constraints=cons
            )
            print(res)
            ret = {
                'xoutermin': res['x'],
                'youtermin': res['fun'],
                'status': repr(res['status']),
                'status_msg': repr(res['message']),
                'x0': x0.tolist()
            }
            return ret
        elif self.solveropt == 'ipopt':
            # IPOPT run
            import ipopt

            lb = np.zeros(self.nweight, dtype=np.float) + 10**-12
            ub = np.ones(self.nweight, dtype=np.float)


            if self.nu >0 and self.OO._metric == 'Egap':
                cl = [self.nweight * self.nu]
                cu = [None]
                ub *= 1.
            elif self.mu == 0:
                # cl = [1.]
                # cu = [1.]
                # ub *= 10.

                cl = []
                cu = []
                ub *= 1.
            elif self.mu > 0 and self.OO._metric != 'Egap':
                cl = [None] * self.nweight
                cu = [0.] * self.nweight
                # cl.append(1.0)
                # cu.append(1.0)
            else:
                raise Exception("Something is wrong here. mu={}, nu={}, doem={}".format(self.mu,self.nu,self.OO._metric))

            nlp = ipopt.problem(
                n=len(x0),
                m=len(cl),
                problem_obj=self.GBO,
                lb=lb,
                ub=ub,
                cl=cl,
                cu=cu
            )

            # IPOPT Options from https://github.com/syarra/pyOpt-pyIPOPT/blob/master/pyIPOPT/pyIPOPT.py
            nlp.addOption('mu_strategy', 'adaptive')
            nlp.addOption('max_iter', 1000)
            # nlp.addOption('recalc_y', 'no')
            # nlp.addOption('recalc_y_feas_tol', 1000.1)
            nlp.addOption('bound_relax_factor',0.)
            # nlp.addOption('constr_viol_tol',10**-12)
            nlp.setProblemScaling(
                obj_scaling=2,
                x_scaling=np.ones(self.nweight, dtype=np.float)
            )
            nlp.addOption('nlp_scaling_method', 'user-scaling')
            # nlp.addOption('max_iter', 20)
            # nlp.addOption('nlp_scaling_method', 'none')
            nlp.addOption('tol', 1e-4)
            # nlp.addOption('derivative_test', 'first-order')
            # nlp.addOption('derivative_test_print_all', 'yes')

            nlp.addOption('output_file', self.logfp)

            # Acceptable convergence options
            nlp.addOption('acceptable_tol', 1e-2)
            nlp.addOption('acceptable_iter', 5)
            nlp.addOption('acceptable_constr_viol_tol', 1e-1)
            nlp.addOption('acceptable_dual_inf_tol', 1e+12)

            x, info = nlp.solve(x0)
            print("Solution of the primal variables: x=%s\n" % repr(x))
            print("Solution of the dual variables: lambda=%s\n" % repr(info['mult_g']))
            print("Objective=%s\n" % repr(info['obj_val']))

            ret = {
                'xoutermin': x,
                'youtermin': info['obj_val'],
                'status': repr(info['status']),
                'status_msg': repr(info['status_msg']),
                'x0': x0
            }
            return ret
        else:
            raise Exception('{solver} solver type not supported'.format(solver=self.solveropt))


class GradientBasedOpt():
    GradObjC: GOptCaller = None
    runs = {}
    curr_p_hat = []
    expcoeff = None
    yintercept = 10**-2

    def __init__(self, *args, GOC, **kwargs):
        self.GradObjC = GOC
        self.windexs = np.array([], dtype=np.int64)
        self.b_in_o_indices = np.array([0], dtype=np.int64)
        index = 0
        i = 0
        for _, v in self.GradObjC.OO._hdict.items():
            self.windexs = np.concatenate((self.windexs, [i] * len(v)))
            self.b_in_o_indices = np.append(self.b_in_o_indices,index+len(v))
            index += len(v)
            i += 1

        if self.GradObjC.mu > 0:
            cumilitivevar = [
                        # np.average
                        sum
                             (self.GradObjC.OO._E[self.b_in_o_indices[i]:self.b_in_o_indices[i + 1]]) for i in
                             range(len(self.b_in_o_indices) - 1)]
            self.expcoeff = np.array(
                [(-1 / (self.GradObjC.mu * csd)) * np.log(self.yintercept) for csd in cumilitivevar])

    def objective(self, x):
        #
        # The callback for calculating the objective
        #
        import autograd.numpy as np
        valarr = [np.round(i, decimals=12) for i in x]
        # valarr = [i for i in x]
        h = hash(tuple(valarr))
        if h in self.runs:
            pass
        else:
            p_hat = self.GradObjC.OO._io_eval(x)[1]
            self.runs[h] = p_hat
        self.curr_p_hat = self.runs[h]

        print('w & p: ',x, self.curr_p_hat)
        print('Elapsed Time = {}'.format(timer()-self.GradObjC.starttime))
        sys.stdout.flush()

        return self._gbo_oo_eval(x)

    def _gbo_oo_eval(self, x):
        return self.GradObjC.OO._oo_eval_direct(x,self.curr_p_hat)

    def gradientAD(self, x):
        #
        # The callback for calculating the gradient
        #
        from autograd import elementwise_grad
        valarr = [np.round(i, 12) for i in x]
        # valarr = [i for i in x]
        h = hash(tuple(valarr))
        if (h in self.runs):
            pass
        else:
            p_hat = self.GradObjC.OO._io_eval(x)[1]
            self.runs[h] = p_hat
        self.curr_p_hat = self.runs[h]

        egrad = elementwise_grad(self._gbo_oo_eval)
        vegrad = [0.]*len(x)
        for anum in range(len(x)):
            y = [0.]*len(x)
            y[anum] = x[anum]
            vegrad[anum] = egrad(y)[anum]
        print('AD', x, vegrad)
        return np.array(vegrad)

    def gradientFD(self, x):
        #
        # The callback for calculating the gradient
        #
        import numdifftools as nd
        valarr = [np.round(i, 12) for i in x]
        # valarr = [i for i in x]
        h = hash(tuple(valarr))
        if (h in self.runs):
            pass
        else:
            p_hat = self.GradObjC.OO._io_eval(x)[1]
            self.runs[h] = p_hat
        self.curr_p_hat = self.runs[h]

        gd = nd.Gradient(self._gbo_oo_eval)
        vgd = np.array(gd(x))
        print('FD', x, vgd)
        return np.array(vgd)

    def gradientANA(self, x):
        #
        # The callback for calculating the gradient
        #
        valarr = [np.round(i, 12) for i in x]
        # valarr = [i for i in x]
        h = hash(tuple(valarr))
        if (h in self.runs):
            pass
        else:
            p_hat = self.GradObjC.OO._io_eval(x)[1]
            self.runs[h] = p_hat
        self.curr_p_hat = self.runs[h]

        grad = np.zeros(self.GradObjC.nweight, dtype=np.float)
        index = 0

        if self.GradObjC.OO._metric == 'D':
            index = 0
            #all_with_no_w = [self.OO._FIM_b(i,self.curr_p_hat) for i in range(n_b)]
            all_with_no_w = self.GradObjC.OO._FIM_b(self.curr_p_hat)
            all_with_w = self.GradObjC.OO._WFIM_b(x,*self.curr_p_hat)
            divsize = [len(h) for h in self.GradObjC.OO._wdict.values()]

            for wi, ws in enumerate(range(self.GradObjC.nweight)):
                no_w = sum(all_with_no_w[index:index + divsize[wi]])
                with_w = sum(all_with_w[index:index + divsize[wi]])
                for pi in range(self.GradObjC.nparam):
                    d = np.zeros((self.GradObjC.nparam, self.GradObjC.nparam), dtype=np.float)
                    for qi in range(self.GradObjC.nparam):
                        if pi == qi:
                            d[qi] = no_w[qi]
                        else:
                            d[qi] = with_w[qi]
                    grad[wi] += np.linalg.det(d)
                grad[wi] *= -1
                index += divsize[wi]
        elif self.GradObjC.OO._metric == 'A':
            all_with_no_w = [self.GradObjC.OO._FIM_b(i, self.curr_p_hat) for i in range(self.GradObjC.nbin)]
            divsize = [len(h) for h in self.GradObjC.OO._wdict.values()]
            for wi, ws in enumerate(range(self.GradObjC.nweight)):
                no_w = sum(all_with_no_w[index:index + divsize[wi]])
                for pi in range(self.GradObjC.nparam):
                    grad[wi] += no_w[pi][pi]
                grad[wi] *= -1
                index += divsize[wi]
        else:
            raise Exception('Analytical gradient not available for {M}. Try ad or fd grad option'.format(M=self.GradObjC.OO._metric))

        # print('ANA', x, grad)
        return grad

    def constraints(self, x):
        import autograd.numpy as np
        #return np.array((np.sum(x)))
        if self.GradObjC.mu == 0 or (self.GradObjC.nu > 0 and self.GradObjC.OO._metric == 'Egap'):
            return self.constructconstr(x)
        else:
            valarr = [np.round(i, 12) for i in x]
            # valarr = [i for i in x]
            h = hash(tuple(valarr))
            if (h in self.runs):
                pass
            else:
                p_hat = self.GradObjC.OO._io_eval(x)[1]
                self.runs[h] = p_hat
            self.curr_p_hat = self.runs[h]

            return self.constructconstr(x)

    def constructconstr(self,x):
        import autograd.numpy as np
        if self.GradObjC.nu > 0 and self.GradObjC.OO._metric == 'Egap':
            return np.array((np.sum(x)))
        elif self.GradObjC.mu == 0:
            return np.array([])

        deviationsum = []
        # for i in range(len(self.b_in_o_indices)-1):
        #     ro = np.array([self.GradObjC.OO._RA[b](self.curr_p_hat) for b in
        #               range(self.b_in_o_indices[i], self.b_in_o_indices[i + 1])])
        #     do = np.array([self.GradObjC.OO._Y[b] for b in
        #                    range(self.b_in_o_indices[i], self.b_in_o_indices[i + 1])])
        #     deviationsum.append(sum((ro - do) ** 2))
        for hn in self.GradObjC.OO._hnames:
            ro = np.array(self.GradObjC.OO.calc_f_val(self.curr_p_hat, sel=self.GradObjC.OO.obsBins(hn)))
            do = np.array([self.GradObjC.OO._Y[b] for b in self.GradObjC.OO.obsBins(hn)])
            deviationsum.append(sum((ro - do) ** 2))



        # for w in range(self.GradObjC.nweight):
        #     rb = self.GradObjC.OO._RA[b](self.curr_p_hat)

        cons = [np.log(x[w]) + (self.expcoeff[w] * deviationsum[w])
                for w in range(self.GradObjC.nweight)]
        # cons.append(sum(x))
            # cons.append(
            #     np.log(x[self.windexs[b]]) + (self.expcoeff[b] * (rb - self.GradObjC.OO._Y[b]) ** 2))
            # cons.append(x[self.windexs[b]] - np.exp(-1 * self.expcoeff[b] * (rb - self.GradObjC.OO._Y[b]) ** 2))
        return np.array(cons)


    def jacobianAD(self, x):
        from autograd import jacobian
        jac = jacobian(self.constructconstr)
        jacv = jac(x)
        # print(jacv)
        return np.array(jacv)

    def jacobianANA(self, x):
        # if self.GradObjC.nu > 0 and self.GradObjC.OO._metric == 'Egap':
        #     print("Analytical Jacobian not available for Egap. Try constrAD instead")
        #     sys.exit(1)
        # elif self.GradObjC.mu == 0:
        #     return np.array([])

        jacv = np.zeros(shape=(len(x),len(x)),dtype=np.float)
        for i in range(len(x)):
            jacv[i][i] = 1/x[i]
        return jacv

    # def hessianstructure(self):
    #     return np.nonzero(np.tril(np.ones((self.wsize, self.wsize))))
    #
    # def hessian(self, x, lagrange, obj_factor):
    #     from autograd import hessian
    #     valarr = [round(i,12) for i in x]
    #     h = hash(tuple(valarr))
    #     if(h in self.runs):
    #         pass
    #     else:
    #         p_hat = self.OO._io_eval(x)[1]
    #         self.runs[h] = p_hat
    #     self.curr_p_hat = self.runs[h]
    #
    #     hess = hessian(self._gbo_oo_eval)
    #     H = obj_factor*hess(x)
    #
    #     #temp - to make it dynamic with dimension of w
    #     H += lagrange[0]*np.array((
    #             (0, 0),
    #             (x[0]+x[1], 0)))
    #
    #     row, col = self.hessianstructure()
    #
    #     return H[row, col]

if __name__ == "__main__":
    from timeit import default_timer as timer
    import os
    import pyoo.outer_objective as oobj
    print("USAGE: python {} indir mu(>=1) doe_metric(A,D,E,Eminplusmax,Emed,D-Egap,Egap,D-Emax) outdir filter_envelope (1 or 0) filter_hypothesis(1 or 0) nu([0.,1.])\n"
          "Use mu=0 to run with no constraint on w, only that w \in [0.,1.]\n"
          "Use nu>0 and doe_metric = Egap to add lower bound constraint on sum of weights\n"
          "Use mu>0 and doe_metric != Egap to add upper bounded by deviation constraint".format(sys.argv[0]))


    indir = sys.argv[1]
    mu = float(sys.argv[2]) if len(sys.argv) > 2 else 1.
    doem = 'D' if len(sys.argv) <= 3 else sys.argv[3]
    outdir = '../experiments/results/gradO_{}'.format(os.path.basename(indir)) \
        if len(sys.argv) <= 4 else sys.argv[4]
    filter_envelope = int(sys.argv[5]) if len(sys.argv) > 5 else 1
    filter_hypothesis = int(sys.argv[6]) if len(sys.argv) > 6 else 1
    nu = float(sys.argv[7]) if len(sys.argv) > 7 else -1.

    apprfile = indir + "/approximation.json"
    datafile = indir + "/experimental_data.json"
    wtfile = indir + "/weights"

    if not os.path.exists(outdir): os.makedirs(outdir,exist_ok=True)
    outfp = outdir + "/out_mu{}_nu{}_dm{}.json".format(repr(mu),nu,doem)

    logdir = "{}/log".format(outdir)
    if not os.path.exists(logdir): os.makedirs(logdir,exist_ok=True)
    logfp = logdir + "/log_mu{}_nu{}_dm{}.log".format(repr(mu),repr(nu),doem)
    start = timer()
    OO = oobj.OuterObjective(wtfile, datafile, apprfile, filter_envelope=filter_envelope,filter_hypothesis=filter_hypothesis,
                             metric=doem, metric_base='WFIM',restart_filter=100, debug=False)
    OO.setNstart(50, 1)
    end = timer()
    fimtime = end - start

    start = timer()
    GO = GOptCaller(OO=OO, mu=mu, nu=nu, OOGRAD='analytic', OOJAC='analytic',LOGFP=logfp,starttime=start)
    ret = GO.run()
    end = timer()
    ootime = end - start
    finnermin, xinnermin, resinner = OO._io_eval(ret['xoutermin'].tolist())
    print("Log file written to {}".format(logfp))
    print("Output file written to {}".format(outfp))
    print("FIM Time = {}. OO time = {}. Total time = {}".format(fimtime,ootime,fimtime+ootime))
    reports = {}
    RR = {
        "X_outer": [ret['xoutermin'].tolist()],  # parameters of outer objective per iteration
        "X_inner": [xinnermin.tolist()],
        # parameters of inner objective per iteration (i.e. tuned parameters given weights)
        "Y_outer": ret['youtermin'],  # Outer objective
        "Y_inner": [finnermin],  # Inner objective
        "OOstartpoint": [ret['x0'].tolist()],
        "OOstartpointcomment": [None],
        "pnames_outer": OO.hnames,  # The parameter names of the oo -- i.e. histogram names
        "pnames_inner": OO.pnames,
        "binids": OO._binids,
        "DOE_METRIC": 'WFIM',
        "DOE_BASE": 'D',
        'log': {
            'fimtime': fimtime,
            'ootime': ootime,
            'oostatus': [ret['status']],
            'oostatus_msg': [ret['status_msg']]
        },
        'WFIM': [OO._WFIM(xinnermin, ret['xoutermin']).tolist()],
    }

    reports['chain-0'] = RR
    import json

    with open(outfp, 'w') as f:
        json.dump(reports, f, indent=4)