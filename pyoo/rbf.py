import numpy as np
import scipy.spatial as scp



class RBF(object):
    """
    Radial basis function interpolation
    """
    def __init__(self, dim, **kwargs):
        self.phifunction = kwargs.get("phifunction", "cubic" )
        self.polynomial  = kwargs.get("polynomial",  "linear")
        self.rbf_weight  = kwargs.get("rbf_weight",  1.0)
        self._dim=dim

        if self.polynomial == 'None':
            self._pdim = 0
        elif self.polynomial == 'constant':
            self._pdim = 1
        elif self.polynomial == 'linear':
            self._pdim = 1 + self.dim
            #self._pdim = self.dim
        else:
            raise Exception("nono!")

    @property
    def pdim(self):
        return self._pdim

    @property
    def dim(self):
        return self._dim

    def fit(self, X, Y):
        """
        Fit RBF for parameters X and data values Y.
        """
        self._X = X
        self._Y = Y
        self._Fbest = np.amin(self._Y)
        self._xbest = np.ravel(self._X[np.argmin(self._Y)])
        self._dim = self._X.shape[1]
        PHI = scp.distance.cdist(self._X, self._X, 'euclidean')
        if self.phifunction == "cubic":
            PHI = PHI**3
        phi0 = self.phi(0) # phi-value where distance of 2 points =0 (diagonal entries)
        P = self.polypart_fit(self._X)

        # Compute RBF parameters
        a_part1 = np.concatenate((PHI, P), axis = 1)
        a_part2 = np.concatenate((np.transpose(P), np.zeros((self.pdim, self.pdim))), axis = 1)
        a = np.concatenate((a_part1, a_part2), axis = 0)

        eta = np.sqrt((1e-16) * np.linalg.norm(a, 1) * np.linalg.norm(a, np.inf))

        _m = self._X.shape[0]
        coeff = np.linalg.solve((a + eta * np.eye(_m + self.pdim)),\
                np.concatenate((self._Y, np.zeros((self.pdim, 1))), axis = 0))

        # rbf parameters
        self._llambda = coeff[0:_m]
        self._ctail = coeff[_m: _m + self.pdim]

    def append(self, X, Y):
        _Y = list(self._Y)
        _Y.append(np.array([Y]))
        self.fit(np.concatenate((self._X, X)),  np.array(_Y))
        # from IPython import embed
        # embed()

    @property
    def xbest(self):
        return self._xbest
    @property
    def Fbest(self):
        return self._Fbest

    @property
    def ctail(self):
        return self._ctail

    @property
    def llambda(self):
        return self._llambda

    def phi(self, r):
        if self.phifunction == 'linear':
            return r
        elif self.phifunction == 'cubic':
            return np.power(r, 3)
        else:
            raise Exception('Error: Unkonwn phi type "%s", exiting.'%self.phifunction)

    def polypart(self, X):
        if self.polynomial == 'None':
                return np.zeros((X.shape[0], 1))
        elif self.polynomial == 'constant':
                return self.ctail * np.ones((X.shape[0], 1))
        elif self.polynomial == 'linear':
                return np.concatenate((np.ones((X.shape[0], 1)), X), axis = 1) * self.ctail
        else:
                raise Exception('Error: Invalid polynomial tail "%s".'%self.polynomial)

    def polypart_fit(self, X):
        if self.polynomial == 'None':
                return np.array([])
        elif self.polynomial == 'constant':
                return np.ones((X.shape[0], 1)), X
        elif self.polynomial == 'linear':
                return np.concatenate((np.ones((X.shape[0], 1)), X), axis = 1)
        else:
                raise Exception('Error: Invalid polynomial tail "%s".'%self.polynomial)

    def create_cands(self, Ncand, pertP=1, sigma_stdev=0.2, xlow=0, xup=1):
        """
        This creates ncands*dim points that are randomly perturbed from the best point.
        #todo: candidate points plus another dimension must add to 1
        (easy: we do know what the full weight vector of xbest is
        -- so we perturb all N values, make them sum 1 and delete last column)
        """
        best = np.zeros(self.dim+1)
        #best = np.zeros(self.dim)
        best[0:self.dim] = self.xbest
        best[self.dim]=1.-np.sum(self.xbest)
        # Ncand times the best value
        cp_e = np.kron(np.ones((Ncand,1)), np.asmatrix(best))
        # This generates random perturbations
        r=np.random.rand(Ncand,self.dim+1) #need dim+1 to account for the "missing" value
        #r = np.random.rand(Ncand, self.dim)  # need dim+1 to account for the "missing" value
        a = r<pertP
        idx= np.where(np.sum(a,axis=1)==0)      
        for ii in range(len(idx[0])):
            f = np.random.permutation(self.dim+1)
            #f = np.random.permutation(self.dim)
            a[idx[0][ii],f[0]] = True
        randnums = np.random.randn(Ncand, self.dim+1)
        #randnums = np.random.randn(Ncand, self.dim)
        randnums[a==False]=0
        pv = randnums*sigma_stdev
        # Create new points by adding random fluctucations to best point
        new_pts = cp_e+pv

        # Iterative, column wise procedure to force the randomly sampled point to be in [0,1]
        import copy
        for ii in range(self.dim+1):
        #for ii in range(self.dim):
            vec_ii = new_pts[:,ii]
            adj_l = np.where(vec_ii < xlow)
            vec_ii[adj_l[0]] = xlow + (xlow - vec_ii[adj_l[0]])
            adj_u = np.where(vec_ii > xup)
            vec_ii[adj_u[0]] = xup - (vec_ii[adj_u[0]]-xup)
            stillout_u = np.where(vec_ii > xup)
            vec_ii[stillout_u[0]] = xlow
            stillout_l = np.where(vec_ii < xlow)
            vec_ii[stillout_l[0]] = xup
            new_pts[:,ii] = copy.copy(vec_ii)

        new_pts = new_pts/np.sum(new_pts, axis =1)

        cp_e = copy.copy(new_pts)
        #rand_pts = np.asmatrix(np.random.uniform(0,1, [Ncand, self.dim+1])) # original
        #rand_pts = np.asmatrix(np.random.uniform(0, 1, [Ncand, self.dim]))
        #cp_r = rand_pts
        #cp_r = rand_pts/np.sum(rand_pts, axis = 1) # original
        cp_r = np.random.dirichlet(np.ones(self.dim+1), Ncand) # simplex
        CandPoint= np.concatenate((cp_e, cp_r), axis =0)
        CandPoint_out = CandPoint[:,0:self.dim] #return only data.dim candidate points
        #CandPoint_out = CandPoint

        return CandPoint_out

    def predict(self, X):
        """
        Get the rbf prediction for points X
        """
        numpoints = X.shape[0] # determine number of candidate points
        # compute pairwise distances between candidates and already sampled points
        dist_val = np.transpose(scp.distance.cdist(X, self._X))
        # compute radial basis function value for distances
        U_Y = self.phi(dist_val)
        # determine the polynomial tail (depending on rbf model)
        RBFvalue = np.asmatrix(U_Y).T * np.asmatrix(self.llambda) + self.polypart(X)

        return RBFvalue, np.asmatrix(dist_val)
