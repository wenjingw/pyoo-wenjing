import time
import copy
import autograd.numpy as np

from apprentice.tools import TuningObjective
from apprentice.rationalapproximation import RationalApproximation
from apprentice.polynomialapproximation import PolynomialApproximation
from pyoo.rational_functions import SymbolicRationalFunction



class OuterObjective(TuningObjective):
    # TODO: Outer objective might inherit from TuningObjective, but should have another TuningObjective for the inner objective as a parameter or similar
    def __init__(self, *args, metric="D", metric_base="FIM", OO_uses_sym_cache=False, hessian=False, Ey=None, **kwargs):
        super().__init__(*args, **kwargs)

        if metric in ["PF", "MEDSCORE", "MEANSCORE"]: self.setAppStructures() # TODO make this pleasant

        self._metric = metric
        self._metric_base = metric_base

        # TODO: use regexp?
        if metric_base == "FIM" or metric_base == "WFIM" or metric_base == "eFIM" or metric_base=="eFIMm" or metric_base == "H":
            hessian = True if (metric_base == "H" or metric_base=="eFIM" or metric_base=="eFIMm") or hessian else False
            print('====================')
            print("Calculation of symbolic derivatives (including hessians if required)...")
            t = time.time()
            # The rational (polynomial) approximations might have the same structure (indeed, it is very likely that many of the approximations have the same underlying rational (polynomial structure)
            # The structure is solely determined by the dimension of the approximation and the orders of the nominator and the denominator
            if OO_uses_sym_cache:
                print("OO is using symbolic approximation cache. This is an experimental feature.")
                idxs_equal1, idxs_equal2 = equal_structures(self._RA)
                idxs_unique = np.unique(idxs_equal1)
                RA_sym_unique = [SymbolicRationalFunction.from_RA(self._RA[iRA], iRA, self, symbolic_coeffs=True, grad=True, hessian=hessian) for iRA in idxs_unique]
                self._RA_sym = [SymbolicRationalFunction.from_sym_RAp(RA_sym_unique[i],RA) for i,RA in zip(idxs_equal2,self._RA)]
            else:
                self._RA_sym = [SymbolicRationalFunction.from_RA(x, index, self, grad=True, hessian=hessian) for index,x in enumerate(self._RA)]
            elapsed = time.time() - t
            print(elapsed)
            print('====================')

            if metric_base == "FIM" or metric_base == "WFIM" or metric_base == "eFIM" or metric_base == "eFIMm":
                # creating SM functions in a loop, check here for more info: https://stackoverflow.com/questions/3431676/creating-functions-in-a-loop
                def make_sm(i):
                    return lambda z: np.array(self._RA_sym[i].eval_grad(*z))[np.newaxis,:]  # sensitivity matrix (as a function of the independent variables) equals the gradient of the rational function as here, the independent variables are the parameters themselves, but transposed, as the sensitivity matrix is SM{{ij} = d y_i / d p_j (thus if there is one output y, the SM is a row vector, whereas the gradient is a column vector)

                self._SM = []
                for i in range(len(self._RA_sym)):
                    self._SM.append(make_sm(i))

                def eval_SM(x):
                    """
                    Evaluate sensitivity matrix for certain parameter value.
                    :param x: Parameter value.
                    :return: Returns sensitivity matrix with dimensions m x n (number of observed quantities x number of parameters)
                    """
                    SM = [np.array(f(x)) for f in self._SM]
                    return np.vstack(SM)
                self._eval_SM = eval_SM

                def FIM_bi(i,x):
                    """
                    FIM contribution of i'ths bin.
                    :param i: Index of bin (i is pythonic, thus it starts at 0).
                    :param x: Parameter point.
                    :return: FIM for bin i.
                    """
                    sigma2 = np.array(1/self._E2[i])
                    SM = np.array(self._SM[i](x))
                    return fim(SM, sigma2)
                self._FIM_bi = FIM_bi
                self._FIM_b = lambda x: [FIM_bi(i, x) for i in range(len(self._binids))]
                def WFIM_bi(i,x,w=None):
                    """
                    Weighted FIM contribution of i'ths bin.
                    :param i: Index of bin (i is pythonic, thus it starts at 0).
                    :param x: Parameter point.
                    :param w: (Optional) Weight for bin (default: uses weight from parenting object).
                    :return: Weighted FIM for bin i.
                    """
                    if w is None:
                        w = np.sqrt(self._W2[i])
                    return w*self._FIM_bi(i,x)
                self._WFIM_bi = WFIM_bi
                def get_WFIM_b(w, *x):
                	return [WFIM_bi(i,x,w[self._windex[i]]) for i in range(len(self._binids))]
                self._WFIM_b = get_WFIM_b

                def eval_FIM(x):
                    """
                    Expected FIM.
                    :param x: Parameter estimate.
                    :return: Expected FIM for whole problem.
                    """
                    FIMs = [self._FIM_bi(i, x) for i in range(len(self._binids))]
                    return sum(FIMs)
                self._FIM = eval_FIM

                def eval_WFIM(x, w=None):
                    """
                    Weighted expected FIM.
                    :param x: Parameter estimate.
                    :param w: (Optional) weights (default: uses weights from parenting object).
                    :return: Weighted expected FIM for whole problem.
                    """
                    n_b = range(len(self._binids))
                    if w is None:
                        w = [np.sqrt(self._W2[i]) for i in n_b]
                    else:
                        w = self._sanity_check_weights(w)
                    WFIMs = [self._WFIM_bi(i, x, wi) for (i,wi) in zip(n_b, w)]
                    return sum(WFIMs)
                self._WFIM = eval_WFIM

                if metric_base == "FIM" or metric_base == "WFIM":
                    def oo_eval_direct(p, w =None):
                        """
                        Evaluate outer objective directly without the inner optimization.
                        :param w: Weights (outer optimization variables).
                        :param p: Physical parameters (inner optimization variables).
                        :return: Outer objective value.
                        """
                        WFIM = self._WFIM(p, w)
                        return -doe_metric(self._metric, WFIM)
                    if metric_base == "FIM":
                        oo_eval_direct = lambda x: oo_eval_direct(x, np.ones(len(self._binids)))
                    self._oo_eval_direct = lambda w, p: oo_eval_direct(p,w)
                elif metric_base == "eFIM" or metric_base=="eFIMm":
                    def D_b(i, x, Ey=None):
                        """
                        Deviation term bin-wise and unweigthed.
                        :return:
                        """
                        if Ey is None:
                            Ey = self._Y[i]
                        m = self._RA_sym[i]
                        y_m = m.eval_f(*x)
                        H_m = m._eval_hessian(x)  # hessian of model
                        return self._E2[i] * (y_m -Ey) * H_m
                    self._D_b = D_b
                    def WD_b(i, x, w=None, Ey=None):  # weighted version
                        if w is None:
                            w = np.sqrt(self._W2[i])
                        return w * self._D_b(i, x, Ey=Ey)
                    self._WD_b = WD_b
                    def eval_D(x, Ey=None):
                        n_b = len(self._binids)
                        if Ey is None:
                            Ey = [self._Y[i] for i in range(n_b)]
                        Ds = [self._D_b(i, x, e) for (i,e) in zip(range(n_b), Ey)]
                        return sum(Ds)
                    self._D = eval_D
                    def eval_WD(x, w=None, Ey=None):
                        n_b = len(self._binids)
                        if w is None:
                            w = [np.sqrt(self._W2[i]) for i in range(n_b)]
                        else:
                            w = self._sanity_check_weights(w)
                        if Ey is None:
                            Ey = [np.sqrt(self._Y[i]) for i in range(n_b)]
                        WDs = [self._WD_b(i, x, W, e) for (i,W,e) in zip(range(n_b), w, Ey)]
                        return sum(WDs)
                    self._WD = eval_WD

                    def eval_eFIM_b(i,x,w=None,Ey=None):
                        FIM = self._WFIM_b(i,x,w)
                        D = self._WD_b(i,x,w,Ey)
                        return FIM - D
                    self._eFIM_b = eval_eFIM_b

                    def eval_eFIM(x,w=None,Ey=None):
                        if w is not None:
                            w = self._sanity_check_weights(w)
                        FIM = self._WFIM(x, w)
                        #FIM = self._FIM(x)
                        D = self._WD(x,w,Ey)
                        return FIM - D
                    self._eFIM = eval_eFIM
                    def oo_eval_direct(p, w =None, Ey=None):
                        """
                        Evaluate outer objective directly without the inner optimization.
                        :param w: Weights (outer optimization variables).
                        :param p: Physical parameters (inner optimization variables).
                        :param Ey: Expected value of data.
                        :return: Outer objective value.
                        """
                        eFIM = self._eFIM(p, w, Ey)
                        return -doe_metric(self._metric, eFIM)
                    self._oo_eval_direct = oo_eval_direct


            elif metric_base == "H": # exact hessian (no sensitivities needed)
                # This is the hessian for the chi squared function only! The chi squared function is defined as x^2(p) = sum_b w_b (r_b(p) - D_b)^2 / sigma_b^2, where b is the index over the bins and p is the parameter
                def H_eval(w,x):
                    """
                    Exact hessian of the chi squared objective function.
                    :param w: Weights.
                    :param x: Parameter point.
                    :return: Hessian (remember, in the current implementation of the chi squared function, there is a factor of 1/2 missing when compared with maximum likelihood estimation).
                    """
                    W = self._sanity_check_weights(w)
                    n = len(self.pnames)
                    Hess = np.zeros((n,n))
                    data = self._Y
                    f = [R.eval_f(*x) for R in self._RA_sym]
                    grad = [R.eval_grad(*x) for R in self._RA_sym]
                    grad2 = [np.matmul(g[:,np.newaxis],g[np.newaxis,:]) for g in grad]
                    hess = [R._eval_hessian(x) for R in self._RA_sym] # if I evaluate the hessian, the gradient will be evaluated as well; thus it should be possible to get rid of the gradient step in the line above
                    for (d,m,G,H,e,w) in zip(data, f, grad2, hess, self._E2, W):
                        Hess += w*e * (G + (m-d)*H)  # remember, the errors are given reciprocal
                    return 2*Hess
                self._H = H_eval

                def oo_eval_direct(w,p):
                    """
                    Evaluate outer objective directly without the inner optimization.
                    :param w: Weights (outer optimization variables).
                    :param p: Physical parameters (inner optimization variables).
                    :return: Outer objective value.
                    """
                    H = self._H(w, p)
                    return -doe_metric(self._metric, H/2) # 1/2 as minus from likelihood was dropped and not negative, as minus from the likelihood was dropped as well
                self._oo_eval_direct = oo_eval_direct


    def setNstart(self, n,nr):
        self._nstart = n
        self._nrestart = nr

    def setLambda(self, lbd):
        self._lbd = lbd

    def _io_eval(self, x):
        # self.setWeights({hn: _x for hn, _x in zip(self.hnames, x)})
        self.setWeights(x)
        # note: np.sqrt(self._W2) should be equal to x
        res = self.minimize(self._nstart,self._nrestart)
        xmin = res["x"]  # minimum of inner optimization
        fmin = res["fun"]
        # the parameter estimate
        return fmin, xmin, res

    def __call__(self, x):

        fmin, p_hat, res = self._io_eval(x) # inner optimization

        y = 0.
        add_quants = {}
        if self._metric == "PF":  # WW edited
            perobs_chi2 = self.meanCont(p_hat, "portfolio")
            y = np.mean(perobs_chi2) + self._lbd * np.std(perobs_chi2)
        else:
            if self._metric == "MEDSCORE":  # WW edited
                y_score = self.meanCont(p_hat, "medscore")  # list of score for each observable
                y = np.sum(y_score)  # total score
            elif self._metric == "MEANSCORE":  # WW edited
                y_score = self.meanCont(p_hat, "meanscore")  # list of score for each observable
                y = np.sum(y_score)  # total score
            elif self._metric_base == "FIM":
                FIM = self._FIM(p_hat)
                y = -doe_metric(self._metric, FIM) # current objective value based on observed FIM
                add_quants["IM"] = FIM
            elif self._metric_base == "WFIM":
                WFIM = self._WFIM(p_hat, x)
                y = -doe_metric(self._metric, WFIM)
                add_quants["IM"] = WFIM
            elif self._metric_base == "eFIM":
                eFIM = self._eFIM(p_hat,x,Ey)
                y = -doe_metric(self._metric, eFIM)
                add_quants["IM"] = eFIM
            elif self._metric_base == "H_OPT": # based on hessian from optimizer
                H_inv = res["hess_inv"].todense()  # BFGS approximates the inverse Hessian, which is the parameter covariance matrix
                y = doe_metric(self._metric,  H_inv/2)  # 1/2 as minus from likelihood was dropped and not negative, as minus from the likelihood was dropped as well
                add_quants["IM"] = H_inv
            elif self._metric_base == "H": # based on true hessian (of the objective function)
                H = self._H(x, p_hat)
                y = -doe_metric(self._metric,  H/2)
                add_quants["IM"] = H

        return y, p_hat, fmin, add_quants

    def _oo_eval(self, x):
        return self(x)[1]

    def _sanity_check_weights(self, w):
        """
        Check weights if number of weights seems to make sense.
        :param w: Weights.
        :return:
        """
        if len(w) == len(self._binids): # we are fine here
            W = w
        elif len(w) == len(self._hdict):
            W = []
            i = 0
            for _, v in self._hdict.items():
                W.extend([w[i] for _ in v])
                i += 1
        elif len(w) == (len(self._hdict)-1):
            raise IndexError("Number of weights does not fit. It might be the case that you forgot to add the last weight to the weights vector, which is calculated from 1 minus the other weights.")
        else:
            raise Exception("Strange number of weights.")
        return np.array(W)




def fim(SM, C, weights=None):
    """
    Returns expected Fisher information matrix in the case of Gaussian independent errors (usually evaluated at a parameter estimate and therefore an approximation to the expected FIM only: nonetheless called expected Fisher information matrix despite being an approximation).
    FIM_{ij} = SM^T C^(-1) SM
    where the sensitivity matrix SM's dimension is m x n (m: number of observed quantities, n: number of parameters), C has dimension m x m (might be a vector -> independent errors), resulting in the FIM's dimension n x n.
    If weights are supplied the observations are weighted, i.e. the number of weights must equal the number of rows in the sensitivity matrix.
    :param SM: Sensitivity matrix.
    :param C: Error covariance matrix.
    :param weights: Weights.
    :return: Expected FIM.
    """
    if not isinstance(SM, np.ndarray) or not isinstance(C, np.ndarray):
        raise Exception("Numpy arrays are required.")
    else:
        m = SM.shape[0]
        #n = SM.shape[1]
        if C.ndim == 0:
            C = C[np.newaxis,np.newaxis]
        elif C.ndim == 1:
            C = np.diag(C)

        if weights is None:
            weights = np.ones(m)

    return np.matmul(np.matmul(weights*np.matrix.transpose(SM), np.linalg.inv(C)), SM)



def doe_metric(doe_type, FIM):
    """
        Calculates metric based on fisher information matrix.

        :param doe_type: Type of DoE metric.
        :param FIM: Fisher information matrix.
        :return: Scalar value of DoE metric.
    """

    def mymedian(a):
        n=len(a)
        if n % 2 != 0:
            return a[int(n/2)]
        return (a[int((n-1)/2)] + a[int(n/2)])/2.0

    if doe_type == "A": # --> equivalent to Eavg as trace is sum_of_eigenvals
        return np.trace(FIM)
    elif doe_type == "D":
        return np.linalg.det(FIM)
    elif doe_type == "E":
        de = np.linalg.eigh(FIM)
        return np.amin(de[0])
    elif doe_type == "Eminplusmax": # DOES NOT SEEM TO WORK (DEPRICATE?)
        de = np.linalg.eigh(FIM)
        return np.amin(de[0]) + np.amax(de[0])
    elif doe_type == "Emed":
        de = np.linalg.eigh(FIM)
        return mymedian(de[0])
    # elif doe_type == "D-Egap":
    elif doe_type == "Egap":
        de = np.linalg.eigh(FIM)
        gap = np.amax(de[0])-np.amin(de[0])
        # return np.linalg.det(FIM)/gap
        return -1*gap
    elif doe_type == "D-Emax":
        de = np.linalg.eigh(FIM)
        return np.linalg.det(FIM) - np.amax(de[0])
    else:
        raise Exception("Specified metric type not known.")


def equal_structures(RAs):
    """
        Searches rational approximations for equal structures in the approximations. Structure of an approximation is solely determined by its dimension and orders of the nominator and the denominator.
        :return 1: Vector with reference to the index for which the corresponding approximation is equal.
        :return 2: Vector with reference to the index for the structurally unique approximations.
        :param RAs: Vector of rational (polynomial) approximations.

        Ex.: First return is [0,0,2,0], i.e. there are four approximations, among which only two different structures exist. The first, second and fourth approximation have all the same structure as the first one. The third approximation has its own structure.
                The second return would be [0,0,1,0] as there are two structurally unique approximations.
    """
    X = []
    for RA in RAs:
        if isinstance(RA,PolynomialApproximation):
            X.append([RA.dim, RA.m, 0])
        elif isinstance(RA,RationalApproximation):
            X.append([RA.dim,RA.m,RA.n])
        else:
            raise TypeError("Type for approximation {} not known.".format(type(RA)))
    idxs_equal1 = np.array([X.index(x) if x in X else x for x in X])
    idxs_equal2 = np.zeros(len(idxs_equal1),dtype='int')
    idxs_unique = np.unique(idxs_equal1)
    idxs_unique_range = np.array(range(len(idxs_unique)))
    for (j,idx) in enumerate(idxs_unique):
        idxs = np.argwhere(idxs_equal1 == idx)
        idxs_equal2[idxs] = idxs_unique_range[j]

    return idxs_equal1, idxs_equal2


