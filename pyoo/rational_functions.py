import numpy as np
import numpy.polynomial.polynomial as poly
import sympy
import itertools
import copy
from operator import itemgetter

from apprentice.rationalapproximation import RationalApproximation
from apprentice.polynomialapproximation import PolynomialApproximation


class RationalFunction():

    def __init__(self, pcoeffs, qcoeffs):
        """
        Construct RationFunction object. The coefficients have to be inserted according to the polynomial package of numpy, see e.g. the polyder function, https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.polynomial.polynomial.polyder.html#numpy.polynomial.polynomial.polyder.
        Example: pcoeffs = [[4,0], [-1,2], [3,0]] is 4-1*x+2*x*y+3*x^2
                y^0 y^1
        x^0     4   0
        x^1     -1  2
        x^2     3   0
        :param pcoeffs:
        :param qcoeffs:
        """
        try:
            pdim = pcoeffs.ndim
            qdim = qcoeffs.ndim
        except:
            raise
        else:
            if (pdim == qdim) and (pdim > 3 or qdim > 3):
                raise Exception(
                    "Currently, the maximum allowed dimension of the numerator or denominator polynomial is 3. The dimension has to be equal for both.")
            self._dim = pdim
            self._pcoeffs = pcoeffs
            self._qcoeffs = qcoeffs
            if pdim == 1:
                fid = poly.polyval
            elif pdim == 2:
                fid = poly.polyval2d
            elif pdim == 3:
                fid = poly.polyval3d
            self._fid = fid  # function identifier, in MATLAB function handle
            self._pcoeffs_grad = [poly.polyder(pcoeffs, axis=a) for a in np.arange(self._dim)]
            self._qcoeffs_grad = [poly.polyder(qcoeffs, axis=a) for a in np.arange(self._dim)]

    def __call__(self, *args):
        return self._fid(*args, self.pcoeffs) / self._fid(*args, self.qcoeffs)

    def grad_eval(self, *args):
        G = np.zeros(self._dim)
        for i in np.arange(self._dim):
            P = self._fid(*args, self.pcoeffs)
            Q = self._fid(*args, self.qcoeffs)
            dP = self._fid(*args, self._pcoeffs_grad[i])
            dQ = self._fid(*args, self._qcoeffs_grad[i])
            G[i] = (dP * Q - P * dQ) / Q ** 2  # quotient rule
        return G

    @property
    def dim(self):
        return self._dim

    @property
    def pcoeffs(self):
        return self._pcoeffs

    @property
    def qcoeffs(self):
        return self._qcoeffs


class SymbolicRationalFunction():
    """
    SymbolicRationalFunction

    Sybolic rational function of the form f(x) = P(x)/Q(x), where P(x) and Q(x) are polynomials made up of arbitrary degree. 'x' might well be a vector resulting in a multivariate rational function.

    Note: Creating 'SymbolicRationalFunction' from sympy.Poly() objects might lead to non-desired behaviour, for the monomials in Poly objects do not have to be monomials in the sense we use them, e.g. you could create a Poly() object from x^2*y/(x+y) where sympy creates a Poly object with the following three monomials: x,y, and 1/(x+y).
    """

    def __init__(self, P, Q, x=None, grad=False, grad_calc="poly", hessian=False, pcoeffs_sym=None, qcoeffs_sym=None):

        try:
            P = sympy.Poly(P)
            Q = sympy.Poly(Q)
        except:
            raise
        else:
            self._f = P / Q
            Pgens = list(P.gens)  # generators, i.e. free variables, of P
            Qgens = list(Q.gens)
            # create independent variable vector
            if x == None:
                x = independent_variables(P,Q) # only the generating ones, i.e. no parameters

            self._x = x
            self._x_P = Pgens
            self._x_Q = Qgens

            dim = len(x)
            self._dim = dim

            params_set = parameters(self._f, x)
            if params_set.issubset(set()): # if parameter set is a subset of an empty set, i.e. empty itself
                params = ()
                self._eval_f = sympy.lambdify(x, self._f, 'numpy')
            else:
                if pcoeffs_sym is None:
                    params = tuple(params_set)
                else:
                    params = pcoeffs_sym + qcoeffs_sym
                param_names = [str(p) for p in params]
                self._params = params
                self._param_names = param_names
                self._eval_f = sympy.lambdify([x,params], self._f, 'numpy')

            self._P = P
            self._Q = Q
            if grad or hessian:
                Paxes, Qaxes = axis_vectors(P, Q, x)
                self._grad, self._eval_grad, gradP, gradQ = gradient(P,Q,Paxes,Qaxes,grad_calc=grad_calc,params=params)
                if hessian:
                    if grad_calc == "poly":
                        self._hessian, self._eval_hessian = hessian_from_rational_funcs(gradP, gradQ, x, params = params)
                    elif grad_calc == "symbolic":
                        print("Please implement.")
                        pass
                else:
                    self._hessian = None; self._eval_hessian = None
            else:
                self._grad = None; self._eval_grad = None
                self._hessian = None; self._eval_hessian = None


    @classmethod
    def from_monomials(cls, pcoeffs, qcoeffs, pmonoms, qmonoms, x=None, **kwargs):

        if len(pcoeffs) != len(pmonoms) or len(qcoeffs) != len(qmonoms):
            raise Exception(
                'Lengths of coefficients and monomials for both numerator and denominator have to be equal.')

        dim = pmonoms.shape[1]
        if qmonoms.shape[1] != dim:
            raise Exception('Dimensions (number of independent variables) have to be equal for both polynomials.')

        if x == None:
            if dim > 3:
                x_names = ','.join([f'x{i}' for i in np.arange(1, dim + 1)])
            else:
                x_names = ["x", "y", "z"]
                x_names = ','.join(x_names[:dim])

            x = sympy.symbols(x_names)
        else:
            if len(x) != dim: raise Exception('Dimension of variable `x` is not equal to dimension of the monomials.')

        # create dicts for polynomial instantiation
        d = monoms2dict(pmonoms, pcoeffs)
        P = sympy.Poly(d, x)
        d = monoms2dict(qmonoms, qcoeffs)
        Q = sympy.Poly(d, x)

        return cls(P, Q, x, **kwargs)

    @classmethod  # from rational or polynomial approximation object, see apprentice pkg, attention: the coefficients are scaled coefficients
    def from_RA(cls, RA, index, OO, symbolic_coeffs = False, **kwargs):

        if OO.use_cache:
            pcoeff = OO._PC[index]
            RA._M = len(pcoeff)
            struct_p = OO._structure
            if type(RA) == PolynomialApproximation:  # coefficients and monomial exponents of the denominator are missing, i.e. create Rational Approximation; nicer would be to define a constructor in RationalApproximation from a PolynomialApproximation
                RA.N = 1
                RA._qcoeff = np.array([1.])
                RA._struct_q = np.array([np.zeros(RA.dim, dtype=int)])  # comment to not use Tunining Objective cache
                qcoeff = RA._qcoeff
                struct_q = RA._struct_q
            else:
                qcoeff = OO._QC[index]
                struct_q = OO._structure
        else:
            pcoeff = RA._pcoeff
            struct_p = RA._struct_p
            if type(RA) == PolynomialApproximation:  # coefficients and monomial exponents of the denominator are missing, i.e. create Rational Approximation; nicer would be to define a constructor in RationalApproximation from a PolynomialApproximation
                RA._qcoeff = np.array([1.])  # uncomment to not use Tuning Objective cache
                RA.N = 1
                RA._struct_q = np.array([np.zeros(RA.dim, dtype=int)])  # uncomment to not use Tunining Objective cache
                qcoeff = RA._qcoeff
                struct_q = OO._structure
            else:
                qcoeff = RA._qcoeff
                struct_q = OO._structure


        if not symbolic_coeffs:

            obj = cls.from_monomials(pcoeff, qcoeff, struct_p, struct_q,**kwargs)

            # we have to adjust the eval function and the gradient as the approximations are in a scaled form.
            obj._scaler = RA._scaler
            eval_f_old = copy.deepcopy(
                obj._eval_f)  # in order to avoid recursion, we have to copy by value not by reference

            def eval_f(*x):
                return eval_f_old(*obj._scaler.scale(x))

            # obj._eval_f = lambda (*x): obj._eval_f(*scaler.scale(x)) # tuple unpacking in a lambda function does not work in python 3
            obj._eval_f = eval_f
            if obj._grad is not None:
                grad_old = copy.deepcopy(
                    obj._grad)  # in order to avoid recursion, we have to copy by value not by reference
                eval_grad_old = copy.deepcopy(
                    obj._eval_grad)  # in order to avoid recursion, we have to copy by value not by reference
                S = obj._scaler._scaleTerm
                obj._grad = S * grad_old

                def eval_grad(*x):
                    return S * eval_grad_old(*obj._scaler.scale(x))

                obj._eval_grad = eval_grad

            if obj._hessian is not None: # accordingly to the gradient we have to adjust the Hessian for the scaled world case
                hessian_old = copy.deepcopy(obj._hessian)
                eval_hessian_old = copy.deepcopy(obj._eval_hessian)
                S = obj._scaler._scaleTerm
                S2 = np.matmul(S[:,np.newaxis], S[np.newaxis,:])
                obj._hessian = S2 * hessian_old

                def eval_hessian(*x):
                    return S2 * eval_hessian_old(*obj._scaler.scale(x))

                obj._eval_hessian = eval_hessian

            return obj
        else: # coefficients have to be symbolic, i.e. variable

            pcoeff_names = ','.join([f'pc{i}' for i in np.arange(RA.M)])
            qcoeff_names = ','.join([f'qc{i}' for i in np.arange(RA.N)])
            pc = sympy.symbols(pcoeff_names) if ',' in pcoeff_names else (sympy.symbols(pcoeff_names),)
            qc = sympy.symbols(qcoeff_names) if ',' in qcoeff_names else (sympy.symbols(qcoeff_names),)
            obj = cls.from_monomials(pc, qc, struct_p, struct_q,pcoeffs_sym=pc,qcoeffs_sym=qc,**kwargs)  # uncomment to not use Tunining Objective cache


            # we have to adjust the eval function and the gradient as the approximations are in a scaled form.
            obj._scaler = RA._scaler
            eval_f_old = copy.deepcopy(obj._eval_f)  # in order to avoid recursion, we have to copy by value not by reference

            def eval_f(x,p):
                return eval_f_old(obj._scaler.scale(x),p)

            # obj._eval_f = lambda (*x): obj._eval_f(*scaler.scale(x)) # tuple unpacking in a lambda function does not work in python 3
            obj._eval_f = eval_f
            if obj._grad is not None:
                grad_old = copy.deepcopy(obj._grad)  # in order to avoid recursion, we have to copy by value not by reference
                eval_grad_old = copy.deepcopy(obj._eval_grad)  # in order to avoid recursion, we have to copy by value not by reference
                S = obj._scaler._scaleTerm
                obj._grad = S * grad_old

                def eval_grad(x,p):
                    return S * eval_grad_old(obj._scaler.scale(x),p)

                obj._eval_grad = eval_grad

            if obj._hessian is not None: # accordingly to the gradient we have to adjust the Hessian for the scaled world case
                hessian_old = copy.deepcopy(obj._hessian)
                eval_hessian_old = copy.deepcopy(obj._eval_hessian)
                S = obj._scaler._scaleTerm
                S2 = np.matmul(S[:,np.newaxis], S[np.newaxis,:])
                obj._hessian = S2 * hessian_old

                def eval_hessian(x,p):
                    return S2 * eval_hessian_old(obj._scaler.scale(x),p)

                obj._eval_hessian = eval_hessian

            return obj

    def from_sym_RAp(self, RA):
        """
        Symbolic rational approximation from generic symbolic rational approximation where algebraic parameters, i.e. without a numeric value, exist. Attention: Only the evaluation functions are changed!
        :param RA: Symbolic rational approximation.
        :param pcoeffs: Coefficients for the numerator polynomial.
        :param qcoeffs: Coefficients for the denominator polynomial.
        :return: Symbolic rational approximation.
        """
        if type(RA) == PolynomialApproximation:
            p = np.concatenate((RA._pcoeff, np.array([1.])))
        else:
            p = np.concatenate((RA._pcoeff, RA._qcoeff))
        # TODO: This is a bit quick-and-dirty as we are simply deep copying the original object and then manipulate its fields as we are only interested in the evaluation functions.
        obj = copy.deepcopy(self)
        def eval_f(*x):
            return self._eval_f(x, p)
        obj._eval_f = eval_f
        if self._eval_grad is not None:
            def eval_grad(*x):
                return self._eval_grad(x, p)
            obj._eval_grad = eval_grad
        if self._eval_hessian is not None:
            def eval_hessian(*x):
                return self._eval_hessian(*x, p)
            obj._eval_hessian = eval_hessian
        return obj



    @property
    def f(self):
        return self._f

    @property
    def grad(self):
        return self._grad

    @property
    def eval_f(self):
        return self._eval_f

    @property
    def eval_grad(self):
        return self._eval_grad

    @property
    def eval_hessian(self):
        return self._eval_hessian

    def is_polynomial(self):
        if self._Q.is_constant():
            return True
        else:
            return False


def monomials_rational(np, nq, dim):
    """
    Monomials of a rational function.

    np:  degree of numerator (maximum sum of exponents)
    nq:  degree of denominator (maximum sum of exponents)
    dim: dimension of problem (number of independent variables)
    """
    return monomials(np, dim), monomials(nq, dim)


def monomials(n,
              dim):  # this basically involves two steps, first creating partitions, and second, permutation of the partitions
    """
    Monomials of a polynomial.

    Parameters
    ----------
    n : int
        degree (maximum sum of exponents)
    dim : int
        dimension of problem (number of independent variables)

    Returns
    -------
    array
        Monomials (array of arrays, unsorted!)

    Note
    ----
    Note for developers: another possibility to create the monomials might be: Use sympy function `sympy.polys.monomials.itermonomials()` that gives you all the monomials for specified variables and degree, see https://docs.sympy.org/latest/_modules/sympy/polys/monomials.html#itermonomials, and after use `as_coeff_exponent`

    """
    sums = np.arange(1, n + 1)
    partitions = np.array([np.zeros(dim, dtype=int)])
    monoms = np.array([np.zeros(dim, dtype=int)])
    for ni in sums:
        parts = sympy.utilities.iterables.ordered_partitions(ni)
        if ni > dim:  # remove partitions greater equal than dim (would be nicer to have an algorithm that does not return those unneeded partitions)
            parts = list(filter(lambda x: len(x) < dim + 1, parts))

        for ps in parts:
            p = np.zeros(dim, dtype=int)  # new partition to add
            p[:len(ps)] = ps
            partitions = np.append(partitions, [p], axis=0)

    for part in partitions[1:]:
        perms = itertools.permutations(
            part)  # better to find a way where only the non-zero elements are permutated, then the unqiuefy step would drop out
        perms = [p for p in perms]
        monoms = np.append(monoms, np.unique(perms, axis=0), axis=0)

    return monoms  # what about specific sorting?


def monoms2C(monoms, coeffs):
    """
    Monomials (array with exponential information) and coefficients to polynomial coefficient matrix as digested by polyder().
    :return:
    """
    emax = np.max(monoms, axis=0)  # maximum exponents
    if sum([isinstance(c,sympy.Expr) for c in coeffs]) > 0: # there are symbolic coefficients
        C = np.zeros(emax + 1, dtype = sympy.Expr)
    else:
        C = np.zeros(emax + 1)
    for m, c in zip(monoms, coeffs):
        C[tuple(np.array(m))] = c

    return C


def C2monoms(C):
    """
    Polynomial coefficient matrix (see e.g. polyder() function) to monomials (i.e. array of exponents) and corresponding coefficients.
    :return:
    """
    idxs = np.nonzero(C)
    n = len(idxs[0])  # number of monomials
    dim = C.ndim  # should be equal to len(idxs)
    monoms = np.zeros((n, dim), dtype=int)
    if sum([isinstance(c.item(), sympy.Expr) for c in np.nditer(C,['refs_ok'])]) > 0:  # there are symbolic coefficients
        coeffs = np.zeros(n, dtype = sympy.Expr)
    else:
        coeffs = np.zeros(n)
    for i in np.arange(n):
        for j in np.arange(dim):
            monoms[i, j] = idxs[j][i]
        coeffs[i] = C[tuple(monoms[i])]
    return monoms, coeffs


def monoms2dict(monoms, coeffs):
    """
    Create dictionary from monomials and corresponding coefficients in order to use them with the Poly() constructor of the sympy package.
    :param monoms:
    :param coeffs:
    :return:
    """
    d = {}
    for (X, Y) in zip(monoms, coeffs):
        d[tuple(X)] = Y
    return d

def parameters(f,gens):
    """
    Get parameters of a symbolic expression, i.e. the difference set of the free symbols and the generating variables.
    :param f: Symbolic expression.
    :param gens: Generating/independent variables of `f`.
    :return: Vector with parameters
    """
    symbols = f.free_symbols # laready returns a set
    return symbols.difference(gens)



def independent_variables(P,Q):
    """
    Returns independent variables from a rational function, i.e only the generating ones. There might be additional parameters.
    :param P: Numerator polynomial.
    :param Q: Denominator polynomial.
    :return: Independent variables of rational function f=P/Q.
    """
    try:
        Pgens = list(P.gens)
        Qgens = list(Q.gens)
    except:
        raise

    x_common = set(Pgens) & set(Qgens)
    idxs_P = sorted([Pgens.index(x) for x in x_common])
    idxs_Q = sorted([Qgens.index(x) for x in x_common])
    if itemgetter(*idxs_P)(Pgens) != itemgetter(*idxs_Q)(Qgens):
        raise Exception("Order of common generators (i.e. free variables) of numerator and denominator polynomials are not equal.")
    else:
        x = []
        Qgens2 = Qgens[:]  # use [:] in order to create a copy (deepcopy) instead of a reference
        for xp in Pgens:
            if any(xp == v for v in x_common):
                for xq in Qgens2:
                    x.append(xq)
                    Qgens2.pop(0)
                    if xq == xp:
                        break
            else:
                x.append(xp)

    return x

def axis_vectors(P,Q,x):
    """
    Axis vectors of the polynomials corresponding to the independent variables.
    :param P:
    :param Q:
    :param x:
    :return:
    """
    try:
        Pgens = list(P.gens)
        Qgens = list(Q.gens)
    except:
        raise

    # Axis vectors for polynomials
    # ============================
    # there might be cases, where the two polynomials do not have the same free variables, e.g. f(x,y) = x*y^2/(x+1)
    # we need to get corresponding axis
    Paxes = -1 * np.ones(len(x), dtype=int)
    Qaxes = -1 * np.ones(len(x), dtype=int)

    for a, v in enumerate(x):
        if v in Pgens:
            Paxes[a] = Pgens.index(v)
        if v in Qgens:
            Qaxes[a] = Qgens.index(v)
    return Paxes, Qaxes

def gradient(P,Q,Paxes,Qaxes,grad_calc="poly", x=None, params=()):
    """

    :param P:
    :param Q:
    :param Paxes:
    :param Qaxes:
    :param grad_calc:
    :return:
    """
    try:
        f = P / Q
    except:
        raise
    else:
        if x == None:
            x = independent_variables(P, Q)

    if grad_calc == "poly":  # use polynomial pkg, i.e. polyder(), as symbolic calculation gets slow when functions get complicated and long
        grad = []
        pmonoms = P.monoms()
        qmonoms = Q.monoms()  # returns only non-zero monomials
        pcoeffs = P.coeffs()
        qcoeffs = Q.coeffs()
        Cp = monoms2C(pmonoms, pcoeffs)
        Cq = monoms2C(qmonoms, qcoeffs)
        gradP = []; gradQ = []
        for (pax, qax) in zip(Paxes, Qaxes):
            if pax >= 0:
                Cdp = poly.polyder(Cp, axis=pax)
            else:
                Cdp = np.array([0.])
            if qax >= 0:
                Cdq = poly.polyder(Cq, axis=qax)
            else:
                Cdq = np.array([0.])
            # back conversion to evaluate it with sympy, as polynomial pkg can only evaluate polynomials up to 3 dimensions, i.e. polyval3d()
            dpmonoms, dpcoeffs = C2monoms(Cdp)
            dqmonoms, dqcoeffs = C2monoms(Cdq)
            dP = sympy.Poly(monoms2dict(dpmonoms, dpcoeffs), P.gens)
            dQ = sympy.Poly(monoms2dict(dqmonoms, dqcoeffs), Q.gens)
            gP = (dP * Q - P * dQ)
            gQ = Q ** 2
            V = gP / gQ # quotient rule
            gradP.append(sympy.Poly(gP)); gradQ.append(sympy.Poly(gQ))
            grad.append(V)

    elif grad_calc == "symbolic":  # symbolic calculation - works, but slow!
        grad = [sympy.diff(f, xi) for xi in x]
        gradP = []; gradQ = []
        for g in grad:
            from sympy import fraction
            #gF = sympy.simplify.radsimp.fraction(g)
            gF = fraction(g)
            gradP.append(gF[0]); gradQ.append(gF[1])

    else:
        raise ValueError("Gradient calculation method not known.")

    if params:
        eval_grad = sympy.lambdify([x, params], grad, 'numpy')
    else:
        eval_grad = sympy.lambdify(x, grad, 'numpy')

    return grad, eval_grad, gradP, gradQ

def hessian_from_rational_funcs(gradP, gradQ, x, params=()):

    n = len(x) # dimension of hessian is nxn
    ni = list(range(n))

    H = sympy.zeros(n)
    Hf = []

    for (gP, gQ, colID) in zip(gradP, gradQ, range(n)):
        xsel = [x[i] for i in ni] # we consider only selected axes, as the hessian is symmetric
        gPaxes, gQaxes = axis_vectors(gP, gQ, xsel)
        ggrad, eval_ggrad, _, _ = gradient(gP, gQ, gPaxes, gQaxes, grad_calc="poly", x=x, params = params)
        H[ni[0]:ni[-1]+1, colID] = ggrad
        Hf.append(eval_ggrad)
        ni.pop(0)

    # add right-upper diagonal entries
    R = sympy.Matrix(np.triu(np.ones(n), 1))
    H2add = sympy.matrix_multiply_elementwise(sympy.transpose(H), R)
    H = H + H2add


    if params:
        def H_eval(x,p):
            H = np.zeros((n,n))
            ni = list(range(n))
            for colID in range(n):
                H_data = Hf[colID](x,p)
                H[ni[0]:ni[-1] + 1, colID] = H_data
                ni.pop(0)

            R = np.ones((n,n)) - np.eye(n)
            return H + np.transpose(H)*R

    else:
        def H_eval(x):
            H = np.zeros((n,n))
            ni = list(range(n))
            for colID in range(n):
                H_data = Hf[colID](*x)
                H[ni[0]:ni[-1] + 1, colID] = H_data
                ni.pop(0)

            R = np.ones((n,n)) - np.eye(n)
            return H + np.transpose(H)*R

    return H, H_eval
