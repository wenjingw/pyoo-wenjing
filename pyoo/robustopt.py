from apprentice.tools import TuningObjective
import autograd.numpy as np
import os
from timeit import default_timer as timer
import os, sys

class ROptCaller(TuningObjective):
    indir = None
    nparam = 0
    nweight = 0
    solveropt = None
    outdir = None
    useweights = None
    useexpvals = None
    mu = None
    RBO = None
    wstartpoint = None
    wstartpointcomment = None
    regparam = 1.


    def __init__(self, *args,
                OOSOLVER='ipopt', OOGRAD='ad',
                useweigts=0, useexpvals=0, mu=1.,meanshift = 0,regparam=None,
                wsp = None, wspc = None, LOGFP='robopt',
                **kwargs):
        super().__init__(*args, **kwargs)
        self.indir = os.path.dirname(args[1])
        self.nparam = self._SCLR.dim
        self.nweight = len(self._hdict)
        self.solveropt = OOSOLVER
        self.logfp = LOGFP
        self.useweights = useweigts
        self.useexpvals = useexpvals
        self.mu = mu
        self.wstartpoint = wsp
        self.wstartpointcomment = wspc
        self.meanshift = meanshift
        self.regparam = float(regparam) if regparam is not None else 1.

        self.run = self.runrobopt

        self.RBO = ROpt(RO=self)
        if OOGRAD == 'ad':
            self.RBO.gradient = self.RBO.gradientAD
            self.RBO.jacobian = self.RBO.jacobianAD
        else:
            raise Exception('{grad} grad type not supported'.format(grad=self.gradopt))

    def runrobopt(self):
        """
        Runs robust optimization using RBO (class:ROpt)
        :return: x0: Starting points used
                 x:  argmin found
                 info: Optimization info including minimum objective
        """
        # Invoke Python based IPOPT
        if self.solveropt == 'cyipopt':
            # order of decision variables: w, p, t
            import ipopt
            nbin = len(self._binids)

            ll = 0. if self.useweights else 1.
            uu = 10. if self.useweights and self.mu == 0 else 1.

            lb = np.concatenate(([ll] * self.nweight, self._SCLR.box[:, 0], [0.] * nbin))
            ub = np.concatenate(([uu] * self.nweight, self._SCLR.box[:, 1], [None] * nbin))

            cl = [None] * 2 * nbin
            cu = [0.] * 2 * nbin

            if self.useweights:
                if self.mu>0:
                    cl.append(self.nweight * self.mu)
                    cu.append(None)
                else:
                    cl.append(1.)
                    cu.append(1.)

            # np.random.seed(23456744)
            np.random.seed(56786549)

            if self.useweights:
                if self.wstartpoint is None:
                    x0 = np.random.uniform(low=0., high=1., size=(self.nweight - 1,))
                    x0 = np.append(x0, 1 - np.sum(x0))
                else:
                    x0arr = self.wstartpoint.split(',')
                    x0 = np.array([float(x) for x in x0arr])
            else:
                x0 = np.array([1.] * self.nweight)
            x0 = np.concatenate((x0.tolist(),
                [np.random.uniform(low=lbv, high=ubv, size=(1,))[0] for lbv, ubv in
                 zip(lb[self.nweight:self.nweight+self.nparam], ub[self.nweight:self.nweight+self.nparam])],
                np.random.uniform(low=0., high=1., size=(nbin,)))
            )
            nlp = ipopt.problem(
                n=len(x0),
                m=len(cl),
                problem_obj=self.RBO,
                lb=lb,
                ub=ub,
                cl=cl,
                cu=cu
            )
            nlp.addOption('mu_strategy', 'adaptive')
            nlp.addOption('max_iter', 1000)
            nlp.setProblemScaling(
                obj_scaling=2,
                x_scaling=np.ones(len(x0), dtype=np.float)
            )
            nlp.addOption('nlp_scaling_method', 'user-scaling')
            nlp.addOption('tol', 1e-7)
            nlp.addOption('output_file', self.logfp)
            x, info = nlp.solve(x0)
            # print("Solution of the primal variables: x=%s\n" % repr(x))
            # print("Solution of the dual variables: lambda=%s\n" % repr(info['mult_g']))
            # print("Objective=%s\n" % repr(info['obj_val']))

            r_w = x[:self.nweight]
            r_p =  x[self.nweight:self.nweight + self.nparam:]
            r_t = x[self.nweight + self.nparam:]
            # TODO: For now there are three rnorms. This is for testing of what works with L curve. Make this only one

            r_resnorm = 0.
            r_resnorm2 = 0.
            r_resnorm3 = 0.
            r_solnorm = 0.
            lbarrnol = [m - sd*sd for m, sd in zip(self._Y, self._E)]
            ubarrnol = [m + sd*sd for m, sd in zip(self._Y, self._E)]
            rball = self.calc_f_val(r_p)
            for binno in range(len(r_t)):
                rb = rball[binno]

                """
                rn option 1
                """
                a = r_w[self.RBO.windexs[binno]] * (rb - self.RBO.lbarr[binno]) ** 2
                b = r_w[self.RBO.windexs[binno]] * (rb - self.RBO.ubarr[binno]) ** 2
                r_resnorm += a if a >= b else b
                """
                rn option 2
                """
                a = r_w[self.RBO.windexs[binno]] * (rb - lbarrnol[binno]) ** 2
                b = r_w[self.RBO.windexs[binno]] * (rb - ubarrnol[binno]) ** 2
                r_resnorm2 += a if a >= b else b

                """
                rn option 3
                """
                r_resnorm3 += r_w[self.RBO.windexs[binno]] * (rb - self._Y[binno]) ** 2

                """
                sn option 1
                """
                r_solnorm += (r_w[self.RBO.windexs[binno]] * rb) ** 2
            return x0[:self.nweight + self.nparam],r_w, r_p, r_t, r_resnorm,r_resnorm2,r_resnorm3,r_solnorm, info
        # Invoke AMPL based IPOPT through PYOMO
        elif self.solveropt == 'ipopt':
           return self.pyomorobustopt()

        else:
            raise Exception('{solver} solver type not supported'.format(solver=self.solveropt))

    def pyomorobustopt(self,solver='ipopt'):
        from pyomo import environ
        def objective(model):
            tsum = 0.
            for i in model.trange:
                tsum += model.tvar[i]
            return tsum

        def tconstraint(model,index,uselb):
            # Calculate r_b
            # Scale p
            if model.toscale:
                scaledp = [model.scaleterm[i]*(model.pvar[i] - model.Xmin[i]) + model.a[i] for i in model.prange]
            else:
                scaledp = [model.pvar[i] for i in model.prange]
            nterm = 0.
            dterm = 0.
            bindex = 0
            for block in model.structure:
                term = 1.
                for pnum in model.prange:
                    term *= scaledp[pnum] ** block[pnum]
                nterm += term * model.PC[index][bindex]
                dterm += term * model.QC[index][bindex]
                bindex+=1
            rb = nterm/dterm
            windex = model.windexes[index]
            if uselb == 1:
                ret = model.wvar[windex] * (rb-model.lbarr[index])**2 - model.tvar[index] <= 0
            else: ret = model.wvar[windex] * (rb-model.ubarr[index])**2 - model.tvar[index] <= 0
            # ret = model.wvar[windex] * rb >=0
            return ret

        def pBound(model, i):
            b = (model.box[i][0], model.box[i][1])
            return b

        def etwineqconstraint(model):
            wsum = 0.
            for i in model.wrange:
                wsum += model.wvar[i]
            return wsum >= model.nweight * model.mu

        def etweqconstraint(model):
            wsum = 0.
            for i in model.wrange:
                wsum += model.wvar[i]
            return wsum == 1

        # concrete model
        model = environ.ConcreteModel()
        model.nweight = self.nweight
        model.wrange = range(self.nweight)
        model.prange = range(self.nparam)
        model.trange = range(len(self._binids))
        model.crange = range(2)
        model.mu = self.mu
        model.lbarr = self.RBO.lbarr
        model.ubarr = self.RBO.ubarr
        model.box = self._SCLR.box.tolist()
        model.windexes = self.RBO.windexs.tolist()
        model.wvar = environ.Var(model.wrange, bounds=(0, 1),initialize=0.5)
        model.pvar = environ.Var(model.prange, bounds=pBound)
        model.tvar = environ.Var(model.trange)
        px0 = []
        for i in model.prange:
            val = np.random.rand()*(model.box[i][1]-model.box[i][0])+model.box[i][0]
            model.pvar[i].value = val
            px0.append(val)
        wx0 = []
        for i in model.wrange:
            val = np.random.rand()
            model.wvar[i].value = val
            wx0.append(val)

        if self.use_cache:
            model.scaleterm = self._SCLR._scaleTerm.tolist()
            model.toscale = False
            for i in model.prange:
                if model.scaleterm[i] != 1.0:
                    model.toscale = True
                    break
            model.Xmin = self._SCLR._Xmin.tolist()
            model.a = self._SCLR._a.tolist()
            model.structure = self._structure.tolist()
            model.PC = self._PC.tolist()
            if(self._hasRationals):
                model.QC = self._QC.tolist()
            else:
                model.QC = self._PC.tolist()
                for num,block in enumerate(model.QC):
                    arr = [0.] * len(block)
                    arr[0] = 1.
                    model.QC[num] = arr
        else:
            print("PYOMO integration with solver {} not implemented without caching in Tuning Objective".format(solver))
            exit(1)
        model.obj = environ.Objective(rule=objective, sense=1)
        model.tconstr = environ.Constraint(model.trange,model.crange, rule=tconstraint)
        if self.mu > 0 and self.mu <= 1:
            model.wconstr = environ.Constraint(rule=etwineqconstraint)
        elif self.mu>1:
            print("mu has to be >0 and <=1. mu was found to be {}\nExiting!".format(self.mu))
            exit(1)
        else:
            model.wconstr = environ.Constraint(rule=etweqconstraint)
        opt = environ.SolverFactory(solver)
        ret = opt.solve(model, tee=True, logfile=self.logfp,keepfiles=True)
        model.pprint()
        ret.write()
        optstatus = {'message': str(ret.solver.termination_condition), 'status': str(ret.solver.status),
                     'time': ret.solver.time, 'error_rc': ret.solver.error_rc}
        print(np.array([model.pvar[i].value for i in model.prange]))
        print(np.array([model.wvar[i].value for i in model.wrange]))
        r_w = np.array([model.wvar[i].value for i in model.wrange])
        r_p = np.array([model.pvar[i].value for i in model.prange])
        r_t = np.array([model.tvar[i].value for i in model.trange])
        x0 = np.array(wx0+px0)
        optstatus = {'message': str(ret.solver.termination_condition), 'status': str(ret.solver.status),
                     'time': ret.solver.time, 'error_rc': ret.solver.error_rc}
        info = {'obj_val': model.obj(), 'status': str(ret.solver.status), 'time': ret.solver.time,
                'error_rc': ret.solver.error_rc, 'status_msg': str(ret.solver.termination_condition)}
        return x0, r_w, r_p, r_t, 0., 0., 0., 0., info


class ROpt():
    RobObjC: ROptCaller = None
    lbarr = None
    ubarr = None
    windexs = None

    def __init__(self, *args, RO=None, **kwargs):
        self.RobObjC = RO

        self.windexs = np.array([],dtype=np.int64)
        i = 0
        for _, v in RO._hdict.items():
            self.windexs = np.concatenate((self.windexs, [i] * len(v)))
            i += 1

        # TODO STATIC/TEMP remove getexpectedvalandvar()
        def getexpectedvalandvar():
            expvalfn = self.RobObjC.indir+"/distr_data.json"
            import json
            with open(expvalfn, 'r') as fn:
                ds = json.load(fn)
            expval = ds['eval']
            var = np.array(ds['sd'])**2

            return expval, var

        # TODO TEMP Both Required???
        if self.RobObjC.useexpvals:
            expval, eps = getexpectedvalandvar()
            self.lbarr = [expval[bno] - (self.RobObjC.regparam * eps[i]) for bno,i in enumerate(self.windexs)]
            self.ubarr = [expval[bno] + (self.RobObjC.regparam * eps[i]) for bno,i in enumerate(self.windexs)]
        else:
            if self.RobObjC.meanshift !=0:
                sindex = abs(self.RobObjC.meanshift)
                if sindex > 9: raise Exception("Meanshift of abs({}) > 9 not allowed".format(self.RobObjC.meanshift))
                mult = self.RobObjC.meanshift/abs(self.RobObjC.meanshift)
                nos  = 10
                varpartitioned = [np.linspace(0,np.abs(sd * sd),nos) for sd in RO._E]

                self.lbarr = [(m + mult * vp[sindex]) - (self.RobObjC.regparam * sd * sd) for m, vp, sd in
                              zip(RO._Y, varpartitioned, RO._E)]
                self.ubarr = [(m + mult * vp[sindex]) + (self.RobObjC.regparam * sd * sd) for m, vp, sd in
                              zip(RO._Y, varpartitioned, RO._E)]
            else:
                self.lbarr = [m - (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]
                self.ubarr = [m + (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]

            # self.lb_inarr = [m - 0.5 * (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]
            # self.lb_outarr = [m - 1.5 * (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]
            #
            # self.ub_outarr = [m + 0.5 * (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]
            # self.ub_inarr = [m + 1.5 * (self.RobObjC.regparam * sd * sd) for m, sd in zip(RO._Y, RO._E)]

            # self.lb_inarr = [v - 0.01 * (sd * sd) for v, sd in zip(self.lbarr, RO._E)]
            # self.lb_outarr = [v + 0.01 * (sd * sd) for v, sd in zip(self.lbarr, RO._E)]
            #
            # self.ub_outarr = [v + 0.01 * (sd * sd) for v, sd in zip(self.ubarr, RO._E)]
            # self.ub_inarr = [v - 0.01 * (sd * sd) for v, sd in zip(self.ubarr, RO._E)]


    # TODO: Commented out for now... Required??? Doing iterative RO?
    # def overwriteconstrbounds(self, newY=None):
    #     self.lbarr = [m - (self.RobObjC.regparam * sd*sd) for m, sd in zip(newY, RO._E)]
    #     self.ubarr = [m + (self.RobObjC.regparam * sd*sd) for m, sd in zip(newY, RO._E)]

    def objective(self, x):
        # order of decision variables: w, p, t
        print(x[:self.RobObjC.nweight], x[self.RobObjC.nweight:self.RobObjC.nweight+self.RobObjC.nparam])
        sys.stdout.flush()
        t = x[self.RobObjC.nweight + self.RobObjC.nparam:]
        return sum(t)

    def gradientAD(self, x):
        from autograd import elementwise_grad
        egrad = elementwise_grad(self.objective)
        vegrad = np.array(egrad(x))
        # print('AD', x, vegrad)
        return np.array(vegrad)

    def constraints(self, x):
        # order of decision variables: w, p, t
        w = x[:self.RobObjC.nweight]
        p = x[self.RobObjC.nweight:self.RobObjC.nweight+self.RobObjC.nparam:]
        t = x[self.RobObjC.nweight + self.RobObjC.nparam:]
        cons = []
        rb = self.RobObjC.calc_f_val(p)
        for binno in range(len(t)):
            cons.append(w[self.windexs[binno]] * (rb[binno] - self.lbarr[binno]) ** 2 - t[binno])
            cons.append(w[self.windexs[binno]] * (rb[binno] - self.ubarr[binno]) ** 2 - t[binno])

            # if not self.RobObjC.useexpvals:
            #     # cons.append(w[self.windexs[binno]] * (rb - self.lb_inarr[binno]) ** 2 - t[binno])
            #     cons.append(w[self.windexs[binno]] * (rb - self.lb_outarr[binno]) ** 2 - t[binno])
            #
            #     # cons.append(w[self.windexs[binno]] * (rb - self.ub_inarr[binno]) ** 2 - t[binno])
            #     cons.append(w[self.windexs[binno]] * (rb - self.ub_outarr[binno]) ** 2 - t[binno])
        if self.RobObjC.useweights:
            cons.append(sum(w))
        return np.array(cons)

    def jacobianAD(self, x):
        from autograd import jacobian
        jac = jacobian(self.constraints)
        vjac = jac(x)
        return np.array(vjac)

if __name__ == "__main__":
    print("USAGE: python {} indir usewts(0/1) useexpvals(1/0) mu(>0 %*100ofObsToBeUsed) meanshit[-9,..,9] outdir filter_envelope (1 or 0) filter_hypothesis(1 or 0) lambda(>0)\n"
          "Use mu=0 to run with constraint e^t*w = 1 (w \in [0.,10.])".format(sys.argv[0]))


    indir = sys.argv[1]
    uwts = int(sys.argv[2]) if len(sys.argv) > 2 else 0
    uexpvals = int(sys.argv[3]) if len(sys.argv) > 3 else 0
    mu = float(sys.argv[4]) if len(sys.argv) > 4 else 1.
    ms = int(sys.argv[5]) if len(sys.argv) > 5 else 0
    outdir = '../experiments/results/robO_{}_w{}_e{}'.format(os.path.basename(indir), repr(uwts), repr(uexpvals))\
        if len(sys.argv) <= 6 else sys.argv[6]
    filter_envelope = int(sys.argv[7]) if len(sys.argv) > 7 else 1
    filter_hypothesis = int(sys.argv[8]) if len(sys.argv) > 8 else 1
    lp = float(sys.argv[9]) if len(sys.argv) > 9 else None


    # indir = '../test_data_min2'
    # indir = '../test_data_3'
    # indir = '../test_data_min21'
    # indir = '../test_data_min22'
    # indir = '../test_data_min23'

    apprfile = indir + "/approximation.json"
    datafile = indir + "/experimental_data_ro.json"
    if not os.path.exists(datafile):
        datafile = indir + "/experimental_data.json"
    wtfile = indir + "/weights"

    if not os.path.exists(outdir): os.makedirs(outdir,exist_ok=True)
    mustr = mu*100
    if mustr == int(mustr):
        mustr = int(mustr)
    outfp = outdir + "/out_mu{}_ms{}_l{}.json".format(mustr,int(ms),repr(lp))

    logdir = "{}/log".format(outdir)
    if not os.path.exists(logdir): os.makedirs(logdir,exist_ok=True)
    logfp = logdir+"/log_mu{}_ms{}_l{}.json".format(mustr,int(ms),repr(lp))

    RO = ROptCaller(wtfile, datafile, apprfile, filter_envelope=filter_envelope,filter_hypothesis=filter_hypothesis,useweigts=uwts,useexpvals=uexpvals,mu=mu,meanshift=ms,regparam=lp,LOGFP=logfp)
    start = timer()
    x0,w, p, t, rnorm, rnorm2,rnorm3, snorm, info = RO.run()
    print(rnorm,rnorm2,rnorm3,snorm)
    end = timer()
    ootime = end - start
    print("Time = {}".format(ootime))
    youtermin = info['obj_val']
    status = repr(info['status'])
    status_msg = repr(info['status_msg'])
    reports = {}
    RR = {
        "X_outer": [w.tolist()],  # parameters of outer objective per iteration
        "X_inner": [p.tolist()],
        # parameters of inner objective per iteration (i.e. tuned parameters given weights)
        "Y_outer": youtermin,  # Outer objective
        "residualnorm1": rnorm,
        "residualnorm2":rnorm2,
        "residualnorm3": rnorm3,
        "solutionnorm":snorm,
        "regparam":lp,
        "Y_inner": None,  # Inner objective
        "OOstartpoint": [x0.tolist()],
        # "OOstartpointcomment": [opts.OOSTARTPOINTC], #TODO uncomment when moving to pyoo-app
        "pnames_outer": RO.hnames,  # The parameter names of the oo -- i.e. histogram names
        "pnames_inner": RO.pnames,
        "binids": RO._binids,
        "DOE_METRIC": None,
        "DOE_BASE": None,
        'log': {
            'fimtime': None,
            'ootime': ootime,
            'oostatus': [status],
            'oostatus_msg': [status_msg]
        }
    }
    reports['chain-0'] = RR
    import json
    with open(outfp, 'w') as f:
        json.dump(reports,f,indent=4)
    print('Solver log file is in:    {}'.format(logfp))
    print('Solution file written to:     {}'.format(outfp))

# for l in 1 0.1 0.01 0.001 0.0001 0.00001 0.000001 0.0000001 0.00000001 0.000000001 0.0000000001 0.00000000001; do nohup python robustopt.py ../test_data_min23 1 $l >"../../log/robo_run_23_"$l".log" 2>&1 & done
    """ USEWEIGHTS = FALSE
    runrobopt
    FOR dir = '../test_data_min2 without noise'
    np.random.seed(23456744):
    STP: [2.79869257 2.04393088]
    MIN: 2.47438708e+00,  1.70684797e+00
    ROB: 0.07490248208640385
    iterations: 10
    time: 871.591536432 *

    np.random.seed(56786549):
    STP [2.3479877 1.3940211]
    MIN 2.47438709e+00,  1.70684799e+00
    ROB: 0.07490248208137225
    iterations: 9
    time: 811.530041133 *
    """

    """ USEWEIGHTS = TRUE
    FOR dir = '../test_data_min2 without noise'
    np.random.seed(23456744):
    STP: [0.833679   0.29291704 0.77672368 0.00999999] [2.81845258 4.44559991]
    MIN: [-9.93254911e-09 -3.22885584e-09  5.25185487e-07  9.99999488e-01] [2.47438759 1.70684842]
    ROB: 4.715308352767664e-06
    iterations: 21
    time: 1834.1352064970001 *

    np.random.seed(56786549):
    STP: [0.4490775  0.10428112 0.15413954 0.29250184] [1.98493828 1.18594746]
    MIN: [-9.93254933e-09 -3.22878184e-09  5.25297696e-07  9.99999488e-01] [2.47438676 1.70684735]
    ROB: 4.715309380544992e-06
    iterations: 25
    time: 2035.879244484  *

    FOR dir = '../test_data_21' model of 5th obs same as 4th obs from '../test_data_min2' but with p = 2.,2.
0 = {str} '/ATLAS_2016_I1419652/d03-x01-y01'
1 = {str} '/ATLAS_2016_I1419652/d04-x01-y01'
2 = {str} '/ATLAS_2016_I1419652/d05-x01-y01'
3 = {str} '/ATLAS_2016_I1467230/d01-x01-y01'
4 = {str} '/ATLAS_2016_I1467230/d01-x01-y01-M'
    np.random.seed(56786549):
    STP: [0.4490775  0.10428112 0.15413954 0.13927533 0.15322651] [1.8731561  3.27735119]
    MIN: [-9.95587858e-09 -7.96784048e-09 -1.80403322e-09 -3.75937131e-09 1.00000002e+00] [1.99999999 2. ]
    ROB: 3.85578076480048e-06
    iterations: 29
    time: 4559.711175981  *

    FOR dir = '../test_data_22' model of 5th obs same as 1st obs from '../test_data_min2' but with p = 2.,2.
0 = {str} '/ATLAS_2016_I1419652/d03-x01-y01'
1 = {str} '/ATLAS_2016_I1419652/d03-x01-y01-M'
2 = {str} '/ATLAS_2016_I1419652/d04-x01-y01'
3 = {str} '/ATLAS_2016_I1419652/d05-x01-y01'
4 = {str} '/ATLAS_2016_I1467230/d01-x01-y01'
    np.random.seed(56786549):
    STP: [0.4490775  0.10428112 0.15413954 0.13927533 0.15322651] Autograd ArrayBox with value [1.8731561  3.27735119]
    MIN: [-9.93254933e-09 -9.95791640e-09 -3.22878244e-09  5.25273314e-07 9.99999498e-01] [2.47438709 1.70684799]
    ROB: 4.214711409394496e-06
    iterations: 27
    time:  3552.3066237979997  *

    FOR dir = '../test_data_23' model of 5th & 6th obs same as 1st obs & 4th from '../test_data_min2' but with p = 2.,2.
0 = {str} '/ATLAS_2016_I1419652/d03-x01-y01'
1 = {str} '/ATLAS_2016_I1419652/d03-x01-y01-M'
2 = {str} '/ATLAS_2016_I1419652/d04-x01-y01'
3 = {str} '/ATLAS_2016_I1419652/d05-x01-y01'
4 = {str} '/ATLAS_2016_I1467230/d01-x01-y01'
5 = {str} '/ATLAS_2016_I1467230/d01-x01-y01-M'
    np.random.seed(56786549):
    STP: [0.4490775  0.10428112 0.15413954 0.13927533 0.04388787 0.10933864] [2.58451781 1.51021283]
    MIN: [-9.93254933e-09 -9.94525263e-09 -3.22878248e-09  5.25273431e-07  9.99999502e-01 -3.75937403e-09] [2.47438709 1.70684799]
    ROB: 3.3555772265552605e-06
    iterations: 17
    time:  3715.17501129  *


    """

    """ USEWEIGHTS = FALSE
        runrobopt
        FOR dir = '../test_data_3'
        np.random.seed(56786549):
        STP [2.3479877 1.3940211]
        MIN 2.43056137e+00,  1.66418100e+00,
        ROB: 0.053443983321831375
        iterations: 14
        time: 1121.842746152 *
    """

    """ USEWEIGHTS = TRUE
        FOR dir = '../test_data_3'
        np.random.seed(56786549):
        STP: [0.4490775  0.10428112 0.15413954 0.29250184][1.98493828 1.18594746]
        MIN: 0.,  1,  0.,  0., 2.43185692e+00,  1.03474021e+00
        ROB: 0.000865960727444184
        iterations: 26
        time: 2084.517230931  *
        """
