import random # WW edited

def simplex(d): # WW edited
    """d-dim simplex  https://cs.stackexchange.com/a/3229/5041
    """
    vec = [0] + sorted([random.random( ) for i in range(d-1)]) + [ 1.0 ]
    for i, v in enumerate( vec[:-1] ):
        vec[i] = vec[i+1] - vec[i]
    vec.pop( -1 )
    return vec

def readWtemp(fname):
    """
    Read in weights file and determine number of non-zero weights
    """
    from collections import OrderedDict
    W = OrderedDict()
    with open(fname, "r") as f:
        for line in f:
            l=line.strip()
            if len(l) == 0 or l.startswith("#"): continue
            _split = l.split()
            if len(_split)>1:
                if float(_split[1]) == 0: continue
            W[_split[0]] = 1
    return W

def mkPlot(_SO, fname):
    """
    A very simple plot
    """
    import pylab
    import numpy as np
    pylab.clf()
    Ybest = _SO.R._Fbest
    Xbest = _SO.R._xbest
    # Training data
    Xtrain = _SO._xtrain
    Ytrain = _SO._ytrain
    Y = _SO.Fbest
    X = range(len(Ytrain+Y))
    pylab.plot(X, Ytrain+Y, label=_SO.useObj)
    pylab.axhline(Ybest, label="Ybest (# %i)"%(Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    pylab.axvline((Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    pylab.axvspan(0,len(Ytrain)+0.5, facecolor='g', alpha=0.5)
    pylab.xlabel("# iterations")
    pylab.ylabel("objective")
    pylab.legend()
    pylab.xlim(xmin=11)
    ymax=Y[0]
    ymin=min(Ytrain+Y)
    pylab.ylim(ymax = ymax + 0.1*(ymax-ymin))
    pylab.ylim(ymin = ymin - 0.1*(ymax-ymin))
    pylab.savefig(fname)



def readTunedParams(fname, names=False):
    with open(fname) as f:
        if names:
            return [      l.strip().split()[0]   for l in f if not l.startswith("#")]
        else:
            return [float(l.strip().split()[-1]) for l in f if not l.startswith("#")]

def readUnitGOF(fname):
    with open(fname) as f:
        for l in f:
            if "UNITGOF" in l:
                return l.split("UNITGOF")[-1].strip()

def mkReport(_SO):
    """
    Store all information of this SO run in a text file.
    """
    Ymin = _SO.R._Fbest
    Xmin = _SO.R._xbest
    # Training data
    Xtrain = _SO._xtrain
    Ytrain = _SO._ytrain

    Ybest = _SO.Fbest # The best value after each iteration --- for step plot
    itMin =(Ytrain + Ybest).index(Ymin) # The step that belongs to the minimum found

    objective = _SO.useObj

    Ysel = _SO.Fsel
    Xsel = _SO.xsel

    # cdir = _SO.objective._prefix
    import glob
    # ihfiles = sorted([x for x in glob.glob("%s/*/ipolhistos.yoda"%cdir)])
    ihbest  = ihfiles[itMin]

    # rfiles   = sorted([x for x in glob.glob("%s/*/results.txt"%cdir)])
    pnames   = readTunedParams(rfiles[0], names=True)
    pvalues  = [readTunedParams(x) for x in rfiles]
    unitgofs = [readUnitGOF(x) for x in rfiles]
    pMin     = pvalues[itMin]

    # import yaml

    # with open(fname, "w") as f:
    return {
            "YMIN":Ymin, "XMIN":Xmin, \
            "XTRAIN":Xtrain, "YTRAIN":Ytrain, \
            "YSEL":Ysel, "XSEL":Xsel, \
            "YBEST":Ybest, "ITMIN":itMin, \
            "OBJECTIVE":objective, "HISTOMIN":ihbest, \
            "PNAMES":pnames, "PMIN":pMin, \
            "PVALUES":pvalues, "UNITGOFS":unitgofs
            }


    # pylab.plot(X, Ytrain+Y, label=_SO.useObj)
    # pylab.axhline(Ybest, label="Ybest (# %i)"%(Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    # pylab.axvline((Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    # pylab.axvspan(0,len(Ytrain)+0.5, facecolor='g', alpha=0.5)
    # pylab.xlabel("# iterations")
    # pylab.ylabel("objective")
    # pylab.legend()
    # pylab.xlim(xmin=11)
    # ymax=Y[0]
    # ymin=min(Ytrain+Y)
    # pylab.ylim(ymax = ymax + 0.1*(ymax-ymin))
    # pylab.ylim(ymin = ymin - 0.1*(ymax-ymin))
    # pylab.savefig(fname)

def mkReportInternal(_SO):
    """
    Store all information of this SO run in a text file.
    """
    Ymin = _SO.R._Fbest
    Xmin = _SO.R._xbest
    # Training data
    Xtrain = _SO._xtrain
    Ytrain = _SO._ytrain

    Ybest = _SO.Fbest # The best value after each iteration --- for step plot
    itMin =(Ytrain + Ybest).index(Ymin) # The step that belongs to the minimum found

    objective = _SO.useObj

    Ysel = _SO.Fsel
    Xsel = _SO.xsel


    return {
            "OBJECTIVE":objective, \
            "X" : _SO._xall, \
            "Y" : _SO._yall, \
            "DESIGNSIZE" : _SO.n0,
            "RISK" : _SO.riskAv
            }


    # pylab.plot(X, Ytrain+Y, label=_SO.useObj)
    # pylab.axhline(Ybest, label="Ybest (# %i)"%(Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    # pylab.axvline((Ytrain + Y).index(Ybest), color="black", linestyle="dashed", linewidth=0.5)
    # pylab.axvspan(0,len(Ytrain)+0.5, facecolor='g', alpha=0.5)
    # pylab.xlabel("# iterations")
    # pylab.ylabel("objective")
    # pylab.legend()
    # pylab.xlim(xmin=11)
    # ymax=Y[0]
    # ymin=min(Ytrain+Y)
    # pylab.ylim(ymax = ymax + 0.1*(ymax-ymin))
    # pylab.ylim(ymin = ymin - 0.1*(ymax-ymin))
    # pylab.savefig(fname)

