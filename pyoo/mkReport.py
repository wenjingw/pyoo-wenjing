#!/usr/bin/env python

from objectives import ProfObj

import os, sys, glob

nskip = 0 if len(sys.argv)<5 else int(sys.argv[4])

rfiles = sorted(glob.glob("%s/*/results.txt"%sys.argv[1]))

_P = ProfObj("")

X = [int(x.split("/")[-2].split("-")[-1]) for x  in rfiles]
Yt = []
Yd = []
for r in rfiles:
    _P.readProfResult(r)
    Yt.append(_P.invcovtrace)
    Yd.append(_P.invcovdet)
import pylab
if sys.argv[3] =="trace":
    pylab.plot(X[nskip:],Yt[nskip:], label="trace")
elif sys.argv[3] =="det":
    pylab.plot(X[nskip:],Yd[nskip:], label="det")
else:
    raise Exception("2nd CL arg must be 'trace' or 'det', you gave %s"%sys.argv[3])
pylab.xlabel("# evaluations")
pylab.ylabel("objective")
pylab.legend()
pylab.savefig(sys.argv[2])
