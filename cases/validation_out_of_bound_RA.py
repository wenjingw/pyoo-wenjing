import os
import numpy as np

p_names = ["pT0Ref", "expPow"]

# true parameter
p0 = np.array([2.47, 1.71])

lbd = 100.



#cmd = "python `which pyoo-app -w ../../pseudodata/weights_for_pseudodata -d {df} {af} --scan-n 200 -M A -IM WFIM`".format(df=data_fp[0], af=approx_fp)
#os.system(cmd) # shouldn't be used anymore, instead, use subprocesses module
res = runfile('/Users/celsloaner/workspace/pyoo/bin/pyoo-app', args=['-w', 'data/sip33_3gw1bwout2p_const_nonoise/weights', '-d', 'data/sip33_3gw1bwout2p_const_nonoise/data.json', 'data/sip33_3gw1bwout2p_const_nonoise/sip_33.json', '--scan-n', '200', '-M', 'A', '--IM', 'eFIMm', '--restart-n', '1', '--lambda', lbd, "--OOstrategy", 'rbf'], wdir=os.getcwd())





raise ValueError("Run the following lines in the console")



print("Lambda: {}".format(lbd))
print("Parameters: {}".format(p_names))
print("True parameter values: {}".format(p0))
print("Optimized outer objective value: {}".format(RR["Y_outer"][-1]))
print("Optimized weights: {}".format(RR["X_outer"][-1]))
print("Optimized inner objective value: {}".format(RR["Y_inner"][-1]))
print("Optimized parameters: {}".format(RR["X_inner"][-1]))



p0 = np.array([2.47, 1.71])
p1 = np.array([3.0, 1.2])
lbins = [len(h) for h in OO._wdict.values()]
n_b = sum(lbins)
Ey = np.zeros(n_b)
for i in range(n_b):
    if i > sum(lbins[0:3])-1:
        Ey[i] = OO._RA_sym[i].eval_f(*p0) + 1.413231648409301
    else:
        Ey[i] = OO._RA_sym[i].eval_f(*p0)


OO.setWeights(RR["X_outer"][-1])
pmin = RR["X_inner"][-1]
OO._oo_eval_direct(pmin, Ey=Ey)

OO.setWeights([1.,1.,1.,0.])
OO._WD(p0,Ey=Ey) # should be zero
OO._oo_eval_direct(p0, Ey=Ey) # that is supposed to be the maximum







print(OO._oo_eval_direct(RR["X_outer"][-1],RR["X_inner"][-1]))
#print(OO._oo_eval_direct(np.ones(n_o), p0))

results_fp = "results/OOOut/report.json"

res = runfile('/Users/celsloaner/workspace/pyoo/bin/pyoo-plot', args=['-t', 'IO_surf', '--plot-lib', 'matplotlib', 'OO.objective', approx_fp, results_fp], wdir=os.getcwd())