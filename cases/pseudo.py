import os
import numpy as np

p_names = ["pT0Ref", "expPow"]

# true parameter
p0 = np.array([2.4743870853827445, 1.7068479855005454])



#cmd = "python `which pyoo-app -w ../../pseudodata/weights_for_pseudodata -d {df} {af} --scan-n 200 -M A -IM WFIM`".format(df=data_fp[0], af=approx_fp)
#os.system(cmd) # shouldn't be used anymore, instead, use subprocesses module
res = runfile('/Users/celsloaner/workspace/pyoo/bin/pyoo-app', args=['-w', 'data/pseudo_4gw0bw2p/weights', '-d', 'data/pseudo_4gw0bw2p/data.json', 'data/pseudo_4gw0bw2p/approx.json', '--scan-n', '200', '-M', 'D', '--IM', 'WFIM', '--restart-n', '1', '--lambda', '0.1', "--OOstrategy", 'rbf'], wdir=os.getcwd())





raise ValueError("Run the following lines in the console")




print("Parameters: {}".format(p_names))
print("True parameter values: {}".format(p0))
print("Optimized outer objective value: {}".format(RR["Y_outer"][-1]))
print("Optimized weights: {}".format(RR["X_outer"][-1]))
print("Optimized inner objective value: {}".format(RR["Y_inner"][-1]))
print("Optimized parameters: {}".format(RR["X_inner"][-1]))

print(OO.objective(RR["X_inner"][-1]))
OO.setWeights([1/3,1/3,1/3,0.])
print(OO.objective(p0))
OO.setWeights(RR["X_outer"][-1])


print(OO._oo_eval_direct(RR["X_outer"][-1],RR["X_inner"][-1]))
#print(OO._oo_eval_direct(np.ones(n_o), p0))

results_fp = "results/OOOut/report.json"

res = runfile('/Users/celsloaner/workspace/pyoo/bin/pyoo-plot', args=['-t', 'IO_surf', '--plot-lib', 'matplotlib', 'OO.objective', approx_fp, results_fp], wdir=os.getcwd())