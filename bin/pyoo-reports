#!/usr/bin/env python

import pyoo as oo
import numpy as np

def readReport(fname):
    import yaml
    with open(fname) as f:
        return yaml.load(f)
def printHISTOMIN(reports):
    print("# All best:")
    for h in sorted([r["HISTOMIN"] for r in reports]):
        print("# " + h)

def getBest(reports):
    ymin=1e10
    bestrep = None
    for num, r in enumerate(reports):
        if r["YMIN"]<ymin:
            ymin=r["YMIN"]
            bestrep=r
    return ymin, bestrep

def mkEvolutionPlots(report, fout, showObjective=True, showTraining=True, showWeights=True, showParams=True, yscale="log"):
    import pylab
    import numpy as np
    XTRAIN = report["XTRAIN"]
    YTRAIN = report["YTRAIN"]

    XMIN   = report["XMIN"]
    YMIN   = report["YMIN"]

    YRUN   = report["YSEL"]
    YBEST  = report["YBEST"]

    OBJ    = report["OBJECTIVE"]
    ITMIN  = report["ITMIN"]

    pylab.clf()
    pylab.title("OO evolution, '%s' objective"%OBJ)

    if showTraining:
        X=range(len(YTRAIN + YBEST))
        pylab.plot(X, YTRAIN+YBEST, label="Best")
        pylab.axvspan(0,len(YTRAIN)+0.5, facecolor='g', alpha=0.1, label="Training")
    else:
        X=range(len(YTRAIN), len(YTRAIN+YBEST))
        pylab.plot(X, YBEST, label="Best")

    pylab.axhline(YMIN, label="Min objective (# %i)"%ITMIN, color="black", linestyle="dashed", linewidth=0.5)
    pylab.xlim(xmin=min(X))
    pylab.yscale(yscale)
    pylab.xlabel("# iterations")
    pylab.legend()
    pylab.savefig(fout)
    pylab.close()

    if showWeights:
        XTRAIN =                report["XTRAIN"]
        XRUN   = [x[0] for x in report["XSEL"] ]
        pylab.clf()
        pylab.title("OO evolution of weights, '%s' objective"%OBJ)
        if showTraining:
            Y = XTRAIN + XRUN
            pylab.axvspan(0,len(YTRAIN)+0.5, facecolor='g', alpha=0.1, label="Training")
        else:
            Y = XRUN

        pylab.plot(X, Y, label="Weights")

        pylab.axvline(X.index(ITMIN), label="Min objective (# %i)"%ITMIN, color="black", linestyle="dashed", linewidth=0.5)
        pylab.ylim((0,1))
        pylab.xlabel("# iterations")
        pylab.legend()
        pylab.savefig(fout.split(".pdf")[0]+"-wevolution.pdf")
        pylab.close()

    if showParams:
        pylab.clf()
        pnames=report["PNAMES"]
        pvals=np.array(report["PVALUES"])
        f, ax = pylab.subplots(len(pnames),1, sharex=True, figsize=(10,len(pnames)*2))
        X=range(len(YTRAIN + YBEST))
        for num, pn in enumerate(pnames):
            ax[num].set_title(pn)
            ax[num].plot(X, pvals[:,num])
            ax[num].axvspan(0,len(YTRAIN)+0.5, facecolor='g', alpha=0.1, label="Training")
            ax[num].axvline(X.index(ITMIN), label="Min objective (%s # %i)"%(OBJ, ITMIN), color="black", linestyle="dashed", linewidth=0.5)
        ax[0].legend()
        ax[-1].set_xlabel("# iterations")
        pylab.savefig(fout.split(".pdf")[0]+"-pevolution.pdf")
        pylab.close()

def mkMinScatter(reports, outdir):
    pnames=reports[0]["PNAMES"]
    ymin = [r["YMIN"] for r in reports]
    xmin = {}
    for num, pn in enumerate(pnames):
        xmin[pn] = [r["PMIN"][num] for r in reports]

    import pylab
    for num, pn in enumerate(pnames):
        X=xmin[pn]
        pylab.clf()
        pylab.plot(X, ymin, "o")
        pylab.xlabel(pn)
        pylab.ylabel(reports[0]["OBJECTIVE"])
        pylab.savefig(os.path.join(outdir, "scatter-%i.pdf"%num))

        pylab.close()



if __name__ == "__main__":
    import optparse, os, sys
    op = optparse.OptionParser(usage=__doc__)
    op.add_option("-v", "--debug", dest="DEBUG", action="store_true", default=False, help="Turn on some debug messages")
    op.add_option("-q", "--quiet", dest="QUIET", action="store_true", default=False, help="Turn off messages")
    op.add_option("--plots", dest="PLOTS", action="store_true", default=False, help="Turn on plotting")
    op.add_option("-o", "--outdir", dest="OUTDIR", default="OOOut", help="The output directory (Default: %default)")
    op.add_option("-r", "--report-filename", dest="RFNAME", default="report.yaml", help="The file names of pyoo reports (Default: %default)")
    op.add_option("-N", dest="NSTART", type=int, default=1, help="The number of startpoints to use (Default: %default)")
    opts, args = op.parse_args()

    import glob
    if len(args)<1:
        raise Exception("Not enough arguments.")
    # INDIR=args[0]

    # report_files = [r for r in glob.glob("%s/*/%s"%(INDIR, opts.RFNAME))]
    # R = [readReport(x) for x in report_files]
    R = readReport(args[0])
    # OUT =[r.rsplit("/",1)[0]+"-report.pdf" for r in report_files]

    printHISTOMIN(R.values())
    ybest, rbest = getBest(R.values())
    # from IPython import embed
    # embed()
    print("# Global best:", rbest['HISTOMIN'])
    print("# Global minimum objective (%s): %f"%(rbest["OBJECTIVE"], ybest))
    print("# Corresponding unitGoF: %s"%rbest["UNITGOFS"][rbest["ITMIN"]])
    print("# params:")
    for k, v in zip(rbest["PNAMES"], rbest["PMIN"]):
        print("%s\t%e"%(k,v))

    if opts.PLOTS:
        for k, rep in R.iteritems():
            mkEvolutionPlots(rep, k+"-report.pdf")

        # mkMinScatter(R.values(), r.split("/",1)[0])

    # from IPython import embed
    # embed()
