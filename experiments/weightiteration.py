#!/usr/env/bin
import numpy as np
import os,sys

# Recipie from http://code.activestate.com/recipes/218332/
def partitions(n):
    # base case of recursion: zero is the sum of the empty list
    if n == 0:
        yield []
        return

    # modify partitions of n-1 to form partitions of n
    for p in partitions(n - 1):
        yield [1] + p
        if p and (len(p) < 2 or p[1] > p[0]):
            yield [p[0] + 1] + p[1:]

# Recipie from http://code.activestate.com/recipes/474124/
def PermutationEnumerator(choice, selection=None):
    if not selection:
        selection = len(choice)

    class Rotor(object):
        def __init__(self, choice, selection, parent=None):
            assert len(choice) >= selection
            self.selection = selection
            self.parent = parent
            self.choice = choice
            self.cursor = 0
            if selection == 1:
                self.child = None
            else:
                self._spawn_child()

        def value(self):
            if self.child:
                result = self.child.value()
                result.append(self.choice[self.cursor])
                return result
            else:
                return [self.choice[self.cursor], ]

        def _spawn_child(self):
            assert self.selection >= 2
            cursor = self.cursor
            child_choice = self.choice[:cursor] + self.choice[cursor + 1:]
            self.child = Rotor(child_choice, self.selection - 1, self)

        def next(self):
            result = False
            if self.child:
                result = self.child.next()

            if result:
                return result
            else:
                self.cursor += 1
                if self.cursor == len(self.choice):
                    return False
                else:
                    if self.child:
                        self._spawn_child()
                    return True

    rotor = Rotor(choice, selection)
    yield rotor.value()
    while rotor.next():
        yield rotor.value()

def createweightvector(args):
    import apprentice
    pstr = " ".join(args.PARAM)
    nstr = "-n " + " ".join(args.NOISE) if len(args.NOISE) != 0 else ""
    bstr = "-b " + " ".join(args.BIAS) if len(args.BIAS) != 0 else ""

    binids, RA = apprentice.tools.readApprox(args.APPROX)
    hnames = sorted(list(set([b.split("#")[0] for b in binids])))
    dim = RA[0].dim
    nweight = len(hnames)

    weightoptions = []

    for part in partitions(10):
        if len(part)>nweight: continue
        arr = part
        for i in range(len(part),nweight):
            arr.append(0)
        weightoptions.append(arr)

    weightvectors = []
    for wo in weightoptions:
        wtemp = []
        for w in PermutationEnumerator(wo, nweight): wtemp.append(w)
        wtemp = np.unique(wtemp, axis=0)
        for w in wtemp: weightvectors.append(w)

    weightvectors = np.unique(weightvectors, axis=0)


    weightdata = {}
    for wno,w in enumerate(weightvectors):
        cmd = "python b_c_r.py -t b -a {a}  -p {p} -m -s {n} {b}". \
            format(a=args.APPROX, p=pstr, n=nstr, b=bstr)
        outdir = os.path.join(args.OUTDIR,str(int(wno+1)))
        cmd += " -o "+outdir
        w=w/10
        wstr = " ".join(w.astype(str))
        cmd += " -w " + wstr
        wstr1 = ", ".join(w.astype(str))
        print(cmd)
        os.system(cmd)
        weightdata[str(int(wno+1))] = w.tolist()

    import json
    with open(os.path.join(args.OUTDIR,"allweights.json"),'w') as f:
        json.dump(weightdata,f,indent=4)

def createweightfilesandordering(args):
    if args.OUTDIR is None:
        raise Exception("Outdir is not specified")
    awfp = os.path.join(args.OUTDIR,"allweights.json")
    if not os.path.exists(awfp):
        raise Exception("All weights file required for creating weight files")
    import json
    with open(awfp,'r') as f:
        wds = json.load(f)
    f.close()
    for key in wds.keys():
        warr = wds[key]
        wstr = ",".join(np.array(warr).astype(str))
        wfp = os.path.join(args.OUTDIR,key,"weightstr.txt")
        with open(wfp,'w') as f:
            f.write("[{}]".format(wstr))
        f.close()

    # Create ordering
    v4arr = [1.0,0.9,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1,0.0]
    order = []
    for v4 in v4arr:
        for k in wds.keys():
            if wds[k][3] == v4:
                order.append(k)
    # print(order)
    # for o in order:
    #     print("{} : {}".format(o,repr(wds[o])))
    wds['order'] = order
    with open(awfp,'w') as f:
        json.dump(wds,f,indent=4)
    f.close()




# for i in {1..286}; do sudo python ../../mkPlotsDM_m_c.299445.py $i/b/appnest/appnest_all/apphood.txt $i/b/appnest/appnest_all/apphoodparams.json "b_all_"_$i weights=`cat $i/weightstr.txt` 1.821725 2.9936 1.03474 4.480053; done

if __name__ == "__main__":
    # python weightiteration.py -a results/b_c_r/sip33_3gw1bwoutbias2p_constnoise/approximation.json -o results/b_c_r/sip33_3gw1bwoutbias2p_constnoise/weightcycle -p 2.47 1.71 -n 0.0005 0.0005 0.0005 0.0005 -b 0 0 0 0.48394232652795577
    # python weightiteration.py -o results/b_c_r/sip33_4gw2p_constnoise/weightcycle -s
    import argparse

    parser = argparse.ArgumentParser(description='Iterate throught weights and create data, and run appnest',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # TODO Currently only one approx file supportted. If multiple use [] and nargs='+',
    parser.add_argument("-a", "--approx", dest="APPROX", type=str, default=None,
                        help="The path for approximation file for type")

    parser.add_argument("-o", "--out", dest="OUTDIR", type=str, default=None,
                        help="The output directory (for type)")

    parser.add_argument("-p", "--param", dest="PARAM", type=str, default=[], nargs='+',
                        help="Parameter to be used. If len(.) == dim(P), same parameter"
                             " will be used for all observable")
    parser.add_argument("-n", "--noise", dest="NOISE", type=str, default=[], nargs='+',
                        help="Noise for each observable")
    parser.add_argument("-b", "--bias", dest="BIAS", type=str, default=[], nargs='+',
                        help="Bias for each observable")

    parser.add_argument("-s", "--createwtstr", dest="WTSTR", action="store_true", default=False,
                        help="Only write weight file. All weight JSON and outdir required")

    args = parser.parse_args()

    if not args.WTSTR:
        createweightvector(args)
    createweightfilesandordering(args)

