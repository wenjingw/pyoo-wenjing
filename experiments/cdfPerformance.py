import numpy as np
import apprentice
import os, sys

def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def checkexists(args):
    k = 0
    type = os.path.basename(args.TESTDIR)
    for t in args.FILES:
        if t == 'u' or t == 'n':
            e = os.path.exists(os.path.join("../../log","optParamsPerObs",type))
            print(e, os.path.join("../../log","optParamsPerObs",type))
            if not e: k = 1
        elif t=='w1':
            print(True, t)
            continue
        else:
            e = os.path.exists(t)
            print(e, t)
            if not e: k = 1
    if k: sys.exit(1)


def buildidealparams(indir):
    import json
    approxfile = args.INDIR + "/approximation.json"
    expdatafile = args.INDIR + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.INDIR + "/experimental_data.json"
    weightfile = args.INDIR + "/weights"

    type = os.path.basename(args.INDIR)
    outdir = os.path.join("../../log","optParamsPerObs",type)
    os.makedirs(outdir,exist_ok=True)

    from apprentice.tools import TuningObjective
    IO = TuningObjective(weightfile, expdatafile, approxfile, filter_hypothesis=False, filter_envelope=False)
    for hname in IO.hnames:
        sel = IO.obsBins(hname)
        res = IO.minimize(nstart=100,nrestart=20, sel=sel)
        x = res['x']
        fx = res['fun']
        w = [1 for i in range(len(IO.hnames))]
        reports = {}
        RR = {
            "X_outer": [w],  # parameters of outer objective per iteration
            "X_inner": [x.tolist()],
            # parameters of inner objective per iteration (i.e. tuned parameters given weights)
            "Y_outer": None,  # Outer objective
            "Y_inner": [fx],  # Inner objective
            "OOstartpoint": [None],
            "OOstartpointcomment": [None],
            "pnames_outer": IO.hnames,  # The parameter names of the oo -- i.e. histogram names
            "pnames_inner": IO.pnames,
            "binids": IO._binids
        }
        reports['chain-0'] = RR
        fn = hname.replace("/", "_")+".json"
        outfile = os.path.join(outdir, fn)
        with open(outfile, 'w') as f:
            json.dump(reports, f, indent=4)


def checkTypes(args):
    import re
    typearrlab = []
    # typearr = []
    # typecleanarr = []
    # typename = []
    hparam = []
    for fnum,r in enumerate(args.FILES):
        if r == 'u':
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('ideal')
            # typename.append('d')
            hparam.append(-1)
        elif r == "n":
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('nadir')
            # typename.append('d')
            hparam.append(-1)
        elif r == "w1":
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('equal weights')
            # typename.append('d')
            hparam.append(-1)
        elif "gradO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            # typename.append('FIM')
            hparam.append(mu)
            if mu == 0:
                # typearr.append('\\multicolumn{1}{|c|}{FIM \\eqref{eq:bilevel_FIM}}')
                typearrlab.append('FIM')
                # typecleanarr.append('FIM without bound')
            else:
                # typearr.append("\\multicolumn{1}{|c|}{\makecell{FIM w/\\\\bound\\eqref{eq:bilevel_FIM_withbound}\\\\$\\mu=%d$}}"%(mu))
                typearrlab.append('bilevel optimization (FIM)')
                # typecleanarr.append('FIM with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum],mu))
        elif "robO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            # typename.append('robustOpt')
            hparam.append(mu)
            # typearr.append("\multicolumn{1}{|c|}{\makecell{Robust\\\\Opt\\eqref{eq:robo_form_final}\\\\$\\mu=%d$}}"%(mu))
            typearrlab.append('robust optimization')
            # typecleanarr.append('Robust Opt with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum], mu))
        else:
            findex = -1
            otherapproaches = ['portfolio', 'medscore', 'meanscore', # Juli/Wenjing's approaches
                               'CTEQ', 'MSTW', 'NNPDF', 'HERA', 'Monash'] # Approaches in A14 paper
            for othernum,other in enumerate(otherapproaches):
                if other in r:
                    findex= othernum
                    break
            if findex == -1: raise Exception("Unknown approach {}".format(r))
            # typename.append(otherapproaches[findex])
            hparam.append(-1)
            # typearr.append(
            #     "\multicolumn{1}{|c|}{%s}" % (otherapproaches[findex]))
            typearrlab.append('{}'.format(otherapproaches[findex]))
            # typecleanarr.append('{}'.format(otherapproaches[findex]))

    # return typearr,typearrlab, typecleanarr, typename, hparam
    return typearrlab,hparam

def plotperformance(args):

    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.tools import TuningObjective
    IO = TuningObjective(weightfile, expdatafile, approxfile, filter_hypothesis=False, filter_envelope=False)
    type = os.path.basename(args.TESTDIR)

    allchi2 = {}
    for fno,file in enumerate(args.FILES):
        chi2perobs = []
        if file == 'u':
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                ufn = hname.replace("/", "_") + ".json"
                ufname = os.path.join("../../log","optParamsPerObs",type,ufn)
                data = readReport(ufname)
                chi2perobs.append(IO.objective(data["x"],sel=sel,unbiased=True)/len(sel))
        elif file == 'w1':
            import json
            nadirfolder = os.path.join("../../log","ConstWeights_W1")
            os.makedirs(nadirfolder,exist_ok=True)
            constwtfile = os.path.join(nadirfolder,"chi2perobs_{}.json".format(os.path.basename(args.TESTDIR)))
            if os.path.exists(constwtfile):
                import json
                with open(constwtfile,'r') as f:
                    ds = json.load(f)
                chi2perobs = ds['chi2perobs']
            else:
                res = IO.minimize(1)
                for hname in IO.hnames:
                    sel = IO.obsBins(hname)
                    chi2perobs.append(IO.objective(res["x"], sel=sel, unbiased=True) / len(sel))
                ds = {}
                ds['chi2perobs'] = chi2perobs
                with open(constwtfile,'w') as f:
                    json.dump(ds,f,indent=4)

        elif file == 'n':
            import json
            nadirfolder = os.path.join("../../log","Nadir")
            os.makedirs(nadirfolder,exist_ok=True)
            nadirfile = os.path.join(nadirfolder,"chi2perobs_{}.json".format(os.path.basename(args.TESTDIR)))
            if os.path.exists(nadirfile):
                import json
                with open(nadirfile,'r') as f:
                    ds = json.load(f)
                chi2perobs = ds['chi2perobs']
            else:
                for hname in IO.hnames:
                    ufn = hname.replace("/", "_") + ".json"
                    ufname = os.path.join("../../log","optParamsPerObs",type,ufn)
                    data = readReport(ufname)
                    remainchi2arr = []
                    for hname2 in IO.hnames:
                        sel2 = IO.obsBins(hname2)
                        remainchi2arr.append(IO.objective(data["x"],sel=sel2, unbiased=True)/len(sel2))
                    chi2perobs.append(np.amax(remainchi2arr))
                ds = {}
                ds['chi2perobs'] = chi2perobs
                with open(nadirfile,'w') as f:
                    json.dump(ds,f,indent=4)
        else:
            data = readReport(file)
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                chi2perobs.append(IO.objective(data["x"],sel=sel,unbiased=True)/len(sel))

        allchi2[file] = chi2perobs

    max = 0
    for key in allchi2:
        if key == 'n':
            continue
        max = np.amax([np.amax(allchi2[key]),max])

    Xaxis = [0]
    i = 1
    val = 2
    while val<max:
        Xaxis.append(val)
        i+=1
        val = 2 ** i
    Xaxis.append(max)

    Yaxis = {}
    Ydiff = {}
    for key in allchi2:
        Yaxis[key] = np.zeros(len(Xaxis))
        Ydiff[key] = [[] for i in range(len(Xaxis))]
        chi2arr = allchi2[key]
        for cno, chi2 in enumerate(chi2arr):
            for xno, x in enumerate(Xaxis):
                if chi2 <= x:
                    Ydiff[key][xno].append(IO.hnames[cno])
                    Yaxis[key][xno] += 1

    typearrlab,hparam = checkTypes(args)
    # COL = ["r", "b", "g", "m", "c", "y", "k"]
    marker = ['-','--','-.',':']

    if not args.TABLE:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(12,10))
        for num,key in enumerate(Yaxis):
            label = typearrlab[num]
            if hparam[num] != -1:
                label += " ($\mu = "+str(hparam[num])+"$)"

            plt.step(Xaxis, Yaxis[key], where='mid', label=label,linewidth=0.2*num+1.5,linestyle="%s"%(marker[num%len(marker)]))
            plt.plot(Xaxis, Yaxis[key], 'C'+str(num)+'o', alpha=0.5)

        size = 20
        plt.xticks(fontsize=size-6)
        plt.yticks(fontsize=size - 6)
        plt.yscale('log')
        plt.legend(fontsize=size)
        plt.xscale('log',basex=2)

        if len(args.TITLE) > 0:
            title = ' '.join(args.TITLE)
            plt.title(title)
        plt.xlabel('$\\tau$', fontsize=size)
        plt.ylabel('$\\left|\\left\\{o| \\frac{\\chi^2_o}{ndf} \\leq \\tau\\right\\}\\right|$',fontsize=size)
        os.makedirs(args.OUTDIR,exist_ok=True)
        plt.savefig(os.path.join(args.OUTDIR, "{}_{}_performance.pdf".format(args.OFILEPREFIX,type)))
        # plt.savefig(os.path.join(args.OUTDIR, "{}_{}_performance.png".format(args.OFILEPREFIX, type)))

#   Comparing the cdf curves
#   K-L Divergence
    from scipy.stats import entropy
    KLdata = {}
    print("KL ==============================")
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1==num2: continue
            e = entropy(Yaxis[key1], Yaxis[key2])
            KLdata[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(e, label1, label2))
    print("\n\n###########################\n\n")

#     Jensen-Shannon distance
    print("JS ==============================")
    from scipy.spatial.distance import jensenshannon
    JSdata = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1==num2: continue
            d = jensenshannon(Yaxis[key1], Yaxis[key2])
            JSdata[key1 + "_" + key2] = d
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(d, label1, label2))
    print("\n\n###########################\n\n")

#   L1 distance
    print("L1 ==============================")
    L1data = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            e = np.sum(np.absolute(Yaxis[key1] - Yaxis[key2]))
            L1data[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(e, label1, label2))
    print("\n \n###########################\n\n")

#   L2 distance
    print("L2 ==============================")
    L2data = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            e = np.sum((Yaxis[key1] - Yaxis[key2])**2)
            L2data[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(e, label1, label2))
    print("\n \n###########################\n\n")

#   \chi2 test
    print("chi2test ==============================")
    chi2testdata = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            e = np.sum([((Yaxis[key1][i] - Yaxis[key2][i]) ** 2)/Yaxis[key1][i] for i in range(1,len(Yaxis[key2]))])
            chi2testdata[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(e, label1, label2))
    print("\n \n###########################\n\n")

#   Area between curve
    print("Area between curve ==============================")
    abc = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            # Using area of Trapezoid to find individual areas and summing them up
            e = sum([(((Yaxis[key1][i] - Yaxis[key2][i]) + (Yaxis[key1][i + 1] - Yaxis[key2][i + 1])) / 2)
                     # * (Xaxis[i + 1] - Xaxis[i])
                     for i in range(len(Yaxis[key1]) - 1)])
            abc[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(e, label1, label2))
    print("\n \n###########################\n\n")

#   KS Test
    from scipy.stats import ks_2samp
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1==num2 or num1>num2: continue
            k = ks_2samp(Yaxis[key1], Yaxis[key2])
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{}\t\t{}\t\t{}".format(k, label1, label2))
    print("\n\n###########################\n\n")

# Anderson-Darling test for k-samples
    from scipy.stats import anderson_ksamp
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            ks, kcv, ksl = anderson_ksamp([Yaxis[key1], Yaxis[key2]])
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            print("{} \t\t{}\t\t{}".format(ks, label1, label2))

    print("\n\n###########################\n\n")
# PRINT TABLES
#     KL 0 and -1
    printtable(args,"KL divergence", KLdata, typearrlab,hparam)

#   JS 0 and -1
    printtable(args, "JS distance", JSdata, typearrlab, hparam)

# #   l1test 0 and -1
#     printtable(args, "$||.||_1$", L1data, typearrlab, hparam)

# #   l2test 0 and -1
#     printtable(args, "$||.||_2^2$", L2data, typearrlab, hparam)

#   chi2test 0 and -1
    printtable(args, "Chi-Square statistic", chi2testdata, typearrlab, hparam)

#   area under curve 0 and -1
    printtable(args, "Area between curve", abc, typearrlab, hparam)


def printtable(args,metric, data, typearrlab,hparam):
    # nums = [0, len(args.FILES) - 1]
    nums = [0]
    s = "%s =============================\n\n"%(metric)
    for num1 in nums:
        s += "\\begin{tabular}{|c|c|}\\hline\n\\textbf{Model} & \\textbf{%s}\\\\\\hline\n"%(metric)
        key1 = args.FILES[num1]
        repvals =[]
        for num2, key2 in enumerate(args.FILES):
            if num1 == num2: continue
            repvals.append(data[key1 + "_" + key2])
        repvals = np.sort(repvals)
        repindex = []
        for val in repvals:
            for num2, key2 in enumerate(args.FILES):
                if num1 == num2: continue
                if data[key1 + "_" + key2] == val:
                    repindex.append(num2)
        for num2 in repindex:
            key2 = args.FILES[num2]
            if num1 == num2: continue
            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            s += "{} & {:.4e} \\\\\\hline\n".format(label2, data[key1 + "_" + key2])
        s+="\\end{tabular}"
        s+="\n\n###########################\n\n"
    print(s)





if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='DoE Performance plot',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-t", "--testdir", dest="TESTDIR", type=str, default=None,
                        help="Directory where testing approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir for the plot file")
    parser.add_argument("--title", dest="TITLE", type=str, default=[], nargs='+',
                        help="Title for the plot (Spaces allowed)")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix (No spaces)")
    parser.add_argument("-p","--paramfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Parameter files in which output is stored and u for including ideal params (seperate files with space). "
                             "Use u for Ideal, n for Nadir and w1 for const. weights. "
                             "If u is used as an option to plot ideal, then please run this script with --onlyideal and -i option first")
    parser.add_argument("--onlycheck", dest="CHECK", default=False, action="store_true",
                        help="Only check for existence of parameter files")
    parser.add_argument("--onlytable", dest="TABLE", default=False, action="store_true",
                        help="Only print data. Dont print plots")
    parser.add_argument("--onlyideal", dest="IDEAL", default=False, action="store_true",
                        help="Only find params for ideal runs. Please provide -i or --indir")
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored "
                             "for building ideal parameters")
    parser.add_argument("-l", "--regparamname", dest="REGPARAMNAME", default=[], nargs='+',
                        help="Regularization param name per file to find in filenames. Ignore option if no regularization "
                             "param used")
    args = parser.parse_args()

    if len(args.REGPARAMNAME) == 0:
        args.REGPARAMNAME = ['mu'] * len(args.FILES)

    print(args)

    if args.IDEAL:
        if args.INDIR is None:
            print("Indur required for ideal param building. Please provide one using -i or --indir option")
            sys.exit(1)
        buildidealparams(args.INDIR)
    elif not args.IDEAL and args.CHECK: checkexists(args)
    else:
        if args.TESTDIR is None:
            print("Testdir required for performance plots. Please provide one using -t or --testdir option")
        checkexists(args)
        plotperformance(args)
