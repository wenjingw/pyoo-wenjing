#!/usr/bin/env python
import pyoo.outer_objective as oobj
import numpy as np
import os, sys


# export w1min=0.1 w1max=0.3 w2min=0.7 w2max=0.9; python paramsweep.py ../test_data_min D WFIM 51 show 1
# export w1min=0 w1max=1 w2min=0 w2max=1; python paramsweep.py ../test_data_min D WFIM 100 show 1

def dosweep(rappfile, datafile, weightfile, doem, doeb, npoints, usejson=0, showorsave='show'):
    boxkeys = ['w1min', 'w1max', 'w2min', 'w2max']
    if not all(k in os.environ for k in boxkeys):
        raise KeyError("Not all box keys present")
    boxvals = {}
    for k in boxkeys:
        boxvals[k] = float(os.environ[k])

    jsonfn = 'results/paramsweep_{m}_{b}_{n}.json'.format(m=doem, b=doeb, n=npoints)
    plotfn = 'results/paramsweep_{m}_{b}_{n}.png'.format(m=doem, b=doeb, n=npoints)
    fnA = 'results/paramsweep_{m}_{b}_{n}A.json'.format(m=doem, b=doeb, n=npoints)
    if (usejson == 0):
        # w1arr = np.linspace(0,1,npoints)
        OO = oobj.OuterObjective(weightfile, datafile, rappfile, metric=doem, metric_base=doeb, restart_filter=100,
                                 debug=False)
        OO.setNstart(1, 1)


        for x in [[0.18,0.82],[0.95,0.05],[0,1],[1,0]]:
            for i in range(10):
                res = OO._io_eval(x)
                p_hat = res[1]
                print(x)
                print(res[0])
                print(p_hat)
                print(str(OO._oo_WFIM(x,p_hat)) + "\n")
            print("=====================")
        exit(1)
        Y = []
        w1arr = np.linspace(boxvals['w1min'], boxvals['w1max'], npoints)

        #############################

        # w2arr = np.linspace(boxvals['w2min'],boxvals['w2max'],npoints)
        # X1,X2 = np.meshgrid(w1arr,w2arr)
        # for w1,w2 in zip(np.ravel(X1),np.ravel(X2)):

        #############################

        w2arr = 1 - w1arr
        X1 = w1arr
        X2 = w2arr
        jsonfn = fnA
        for w1, w2 in zip(X1, X2):
            #############################

            x = [w1, w2]
            print(x)
            p_hat = OO._io_eval(x)[1]
            Y.append(OO._oo_WFIM(x, p_hat))

        # Y = np.reshape(np.array(Y), [len(X1), len(X2)])
        # Y = np.reshape(np.array(Y),np.shape(X1))

        data = {}
        data['X1'] = np.ravel(X1).tolist()
        data['X2'] = np.ravel(X2).tolist()
        data['Y'] = Y
        data['shape'] = np.array(np.shape(X1)).tolist()

        import json
        with open(jsonfn, 'w') as f:
            json.dump(data, f, indent=4)
    else:
        # from IPython import embed
        # embed()
        import json
        with open(jsonfn, 'r') as f:
            datastore = json.load(f)

        import matplotlib.pyplot as plt
        from mpl_toolkits import mplot3d
        # plt.plot3D(X1,X2,Y)

        fig = plt.figure()
        ax = plt.axes(projection='3d')
        X1 = datastore['X1']
        X2 = datastore['X2']
        Y = datastore['Y']
        shape = tuple(datastore['shape'])
        X1 = np.reshape(X1, shape)
        X2 = np.reshape(X2, shape)
        Y = np.reshape(Y, shape)
        # print(np.shape(X1))

        # ax.plot_surface(X1, X2, Y,alpha=0.8)
        ax.contour3D(X1, X2, Y, 100, cmap='viridis', alpha=0.8)

        X1 = np.ravel(X1)
        X2 = np.ravel(X2)
        Y = np.ravel(Y)

        # w1r = np.linspace(boxvals['w1min'],boxvals['w1max'],npoints)
        # w2r = 1-w1r

        if os.path.isfile(fnA):
            import json
            with open(fnA, 'r') as f:
                cds = json.load(f)
            X11 = cds['X1']
            X22 = cds['X2']
            Y11 = cds['Y']
            X1cons = []
            X2cons = []
            Ycons = []
            index = 0
            for w1, w2 in zip(X11, X22):
                if w1 < boxvals['w1min'] or w1 > boxvals['w1max'] or w2 < boxvals['w2min'] or w2 > boxvals['w2max']:
                    index += 1
                    continue

                X1cons.append(w1)
                X2cons.append(w2)
                Ycons.append(Y11[index])
                index += 1
            ax.plot3D(X1cons, X2cons, Ycons, 'k--')
        ax.set_xlabel('w1')
        ax.set_ylabel('w2')
        ax.set_zlabel('Outer Objective')
        r1 = np.intersect1d(np.where(X1 == 1), np.where(X2 == 0))
        r2 = np.intersect1d(np.where(X1 == 0), np.where(X2 == 1))
        print(X1[r1], X2[r1], Y[r1])
        print(X1[r2], X2[r2], Y[r2])
        minx1 = X1[np.argmin(Y)]
        minx2 = X2[np.argmin(Y)]
        miny = Y[np.argmin(Y)]
        if doem == "D":
            if 0.21 > boxvals['w1min'] and 0.21 < boxvals['w1max'] and 0.79 > boxvals['w2min'] and 0.79 < boxvals['w2max']:
                ax.scatter(0.21, 0.79, -4.06 * 10 ** 5, color='red')
            if 0.05 > boxvals['w1min'] and 0.05 < boxvals['w1max'] and 0.95 > boxvals['w2min'] and 0.95 < boxvals['w2max']:
                ax.scatter(0.05, 0.95, -5.53E+05, color='magenta', marker="*")
        elif doem == 'A':
            if 1 > boxvals['w1min'] and 1 < boxvals['w1max'] and 0 > boxvals['w2min'] and 0 < boxvals['w2max']:
                ax.scatter(1, 0, -2.48 * 10 ** 3, color='red')
            if 0.05 > boxvals['w1min'] and 0.05 < boxvals['w1max'] and 0.95 > boxvals['w2min'] and 0.95 < boxvals['w2max']:
                ax.scatter(0.95, 0.05, -2.3 * 10 ** 3, color='magenta', marker="*")

        # ax.set_title("{m} {b} {n}\n ({x11},{x12}: {y1}); \n({x21},{x22}: {y2})".format(m=doem,b=doeb,n=npoints,x11=X1[r1][0],x12=X2[r1][0],x21=X2[r1][0],x22=X2[r2][0],y1=Y[r1][0],y2=Y[r2][0]))
        # ax.set_title("{m} {b} {n}\n ({x11},{x12}: {y1})".format(m=doem,b=doeb,n=npoints,x11=minx1,x12=minx2,y1=miny))
        ax.set_title("{m} {b} {n}".format(m=doem, b=doeb, n=npoints, x11=minx1, x12=minx2, y1=miny))
        # from IPython import embed
        # embed()
        # ax.plot3D(X1, X2, Y)
        ax.view_init(elev=1., azim=-138)
        if showorsave == 'show':
            plt.show()
        elif showorsave == 'save':
            plt.savefig(plotfn)


if __name__ == "__main__":

    if len(sys.argv) != 7:
        print("Usage: {} infolder doem doeb npoints_per_dimension usejson showorsave".format(sys.argv[0]))
        sys.exit(1)

    if not os.path.exists(sys.argv[1]):
        print("Input folder '{}' not found.".format(sys.argv[1]))
        sys.exit(1)

    indir = sys.argv[1]
    doem = sys.argv[2]
    doeb = sys.argv[3]
    npoints = int(sys.argv[4])
    showorsave = sys.argv[5]
    usejson = int(sys.argv[6])

    weightfile = "{IN}/weights".format(IN=indir)
    datafile = "{IN}/experimental_data.json".format(IN=indir)
    rappfile = "{IN}/approximation.json".format(IN=indir)

    dosweep(rappfile, datafile, weightfile, doem, doeb, npoints, usejson, showorsave)

    sys.exit(0)
