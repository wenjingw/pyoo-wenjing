
from apprentice.tools import TuningObjective
import numpy as np


def longestsubarrayatmostk(arr, karr):
    n = len(arr)
    sum = 0
    maxcount = 0
    maxstartindex = 0
    maxendindex = 0
    changestartto = 0

    for i in range(n):
        if maxcount + 1 < n and (sum + arr[i]) <= karr[maxcount + 1]:
            sum += arr[i]
            maxcount += 1
            maxstartindex = changestartto
            maxendindex = i
        elif sum != 0:
            sum = sum - arr[i - maxcount] + arr[i]
            changestartto = i - maxcount + 1
    if maxstartindex > 0 and arr[maxstartindex - 1] < arr[maxendindex]:
        maxendindex -= 1
        maxstartindex -= 1
    elif maxendindex < n-1 and arr[maxstartindex] > arr[maxendindex + 1]:
        maxendindex += 1
        maxstartindex += 1
    return maxcount, maxstartindex, maxendindex


if __name__ == "__main__":
    # For testing function longestsubarrayatmostk
    arr = [1, 2, 1, 3, 3,5,5]
    # arr = [2, 2, 0, 0, 1, 5, 5]
    # arr = [2,5,0,0,1,5,0,0,0,5,3]
    arr = [2, 3, 0, 0, 0, 0, 0, 0, 2, 3]
    # arr = [3, 2, 0, 0, 0, 0, 0, 0, 3, 3]
    n = len(arr)
    k = 4
    # print(longestsubarrayatmostk(arr, n, k))
    # exit(1)


    import os,sys

    if len(sys.argv) == 3:
        indir = sys.argv[1]
        alpha = float(sys.argv[2])
        apprfile = indir + "/approximation.json"
        datafile = indir + "/experimental_data.json"
        wtfile = indir + "/weights"
    elif len(sys.argv) == 5:
        apprfile = sys.argv[1]
        datafile = sys.argv[2]
        wtfile = sys.argv[3]
        alpha = float(sys.argv[4])
    else:
        print("Incorrect number of arguments")
        print('USAGE: python {} indirectory alpha'.format(sys.argv[0]))
        print('OR')
        print('USAGE: python {} approximation_file data_file weights_file alpha'.format(sys.argv[0]))
        sys.exit(1)


    # ###############
    # # TEMP
    # file = '../../log/robO/A14/out_mu22.1_ms0_lNone.json'
    # import json
    # with open(file,'r') as f:
    #     ds = json.load(f)
    # wts = ds['chain-0']['X_outer'][0]
    # pnames_outer = ds['chain-0']['pnames_outer']
    # ###############

    # https://stackoverflow.com/questions/32301698/how-to-build-a-chi-square-distribution-table
    from scipy.stats import chi2
    # chi2_critical = chi2.isf(p, df)

    IO = TuningObjective(wtfile, datafile, apprfile)
    nstart = 20
    nrestart = 10
    cnt = 0
    for hn in IO._hnames:
        sel = IO.obsBins(hn)
        res = IO.minimize(nstart=nstart,nrestart=nrestart,sel=sel)
        # chi2_test = res['fun']
        param = res['x']
        rbvals = IO.calc_f_val(param, sel=sel)
        chi2_test_arr = (rbvals -  IO._Y[sel]) ** 2 * IO._E2[sel]
        chi2_test = sum(chi2_test_arr)

        from scipy.stats import chi2
        df_critical = len(sel) - len(param)
        chi2_critical = chi2.isf(alpha,df_critical)
        if chi2_test > chi2_critical:
            chi2_critical_arr = np.zeros(len(sel))
            for i in range(len(param)+1):
                chi2_critical_arr[i] = np.Infinity
            for i in range(len(param)+1,len(sel)):
                chi2_critical_arr[i] = chi2.isf(alpha,i-len(param))
            bincount, binstartindex, binendindex = longestsubarrayatmostk(arr=chi2_test_arr,karr=chi2_critical_arr)
            # TODO: Check this special case
            if bincount<len(param)+1:
                # print("chi2_test = {} and chi2_critical = {}".format(chi2_test, chi2_critical))
                # print(bincount, binstartindex, binendindex)
                if sum(chi2_test_arr[binstartindex:binendindex+1]) > chi2_critical:
                    bincount = 0
                    binstartindex = -1
                    binendindex = -1
                else:
                    chi2_critical_arr = [chi2_critical] * len(sel)
                    bincount, binstartindex, binendindex = longestsubarrayatmostk(arr=chi2_test_arr,
                                                                                  karr=chi2_critical_arr)


            print("For obs {} with {} bins, max count = {}, chi2 critical val is {}".format(hn,len(sel),bincount, chi2_critical_arr[bincount]))
            print("Discard bins {}-{} ({}), keep bins {}-{} ({}), discard bins {}-{} ({})\n".format(0,binstartindex,sum(chi2_test_arr[0:binstartindex]),
                                                                                  binstartindex,binendindex, sum(chi2_test_arr[binstartindex:binendindex+1]),
                                                                                  binendindex+1,len(sel),sum(chi2_test_arr[ binendindex+1:len(sel)])))

            cnt += 1

    print("Ch2 test: Modified {} observables out of {}".format(cnt,len(IO._hnames)))


