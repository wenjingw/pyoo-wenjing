import sys,os
import numpy as np
def makehypothesisfiltereddata(indir,outdir):
    import json
    with open(indir + "/keepids.json", 'r') as f:
        keepids = json.load(f)

    with open(indir + "/approximation.json", 'r') as f:
        apprx = json.load(f)

    with open(indir + "/experimental_data.json", 'r') as f:
        expdata = json.load(f)

    newapprx = {}
    for k in apprx:
        if k in keepids:
            newapprx[k] = apprx[k]

    newdata = {}
    for k in expdata:
        if k in keepids:
            newdata[k] = expdata[k]

    wstr = ""
    hnames = [b.split("#")[0] for b in keepids]
    uniquehnames = np.unique(hnames)
    for hn in uniquehnames:
        wstr += "{}\t1\n".format(hn)

    os.makedirs(outdir, exist_ok=True)
    with open(outdir + "/approximation.json", 'w') as f:
        json.dump(newapprx, f, indent=4)

    with open(outdir + "/experimental_data.json", 'w') as f:
        json.dump(newdata, f, indent=4)

    with open(outdir + "/weights", 'w') as f:
        f.write(wstr)

def makeoutlierfiltereddata(indir):
    import json
    with open(indir + "/approximation.json", 'r') as f:
        apprx = json.load(f)

    with open(indir + "/experimental_data.json", 'r') as f:
        expdata = json.load(f)

    import apprentice
    matchers = apprentice.weights.read_pointmatchers(indir + "/weights")
    weights = [(m, wstr) for m, wstr in matchers.items()]
    newapprx = {}
    newdata = {}
    for w in weights:
        for k in apprx.keys():
            if w[0].match_path(k):
                newapprx[k] = apprx[k]
                newdata[k] = expdata[k]
    with open(indir + "/approximation.json", 'w') as f:
        json.dump(newapprx, f, indent=4)

    with open(indir + "/experimental_data.json", 'w') as f:
        json.dump(newdata, f, indent=4)


if __name__ == "__main__":
    indir = sys.argv[1]
    outdir = sys.argv[2]
    if outdir == "outlier":
        makeoutlierfiltereddata(indir)
    else:
        makehypothesisfiltereddata(indir,outdir)




