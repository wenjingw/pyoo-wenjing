import os,sys
def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return D, {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def checkexists(args):
    k = 0
    for t in args.FILES:
        e = os.path.exists(t)
        print(e, t)
        if not e: k = 1
    if k: sys.exit(1)

def buildexacthessian(args):
    approxfile = args.INDIR + "/approximation.json"
    expdatafile = args.INDIR + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.INDIR + "/experimental_data.json"
    weightfile = args.INDIR + "/weights"

    from pyoo.outer_objective import OuterObjective
    # metric is ASIS so that doe_metric is not accidentally called. No metric called "ASIS" exists in outerObjective
    OO = OuterObjective(weightfile, expdatafile, approxfile, metric="ASIS",metric_base="H",
                        filter_hypothesis=False, filter_envelope=False,OO_uses_sym_cache=True)
    typein = os.path.basename(args.INDIR)
    os.makedirs(args.OUTDIR, exist_ok=True)
    import json
    for fno, file in enumerate(args.FILES):
        typefile = os.path.splitext(os.path.basename(file))[0]
        D,data = readReport(file)
        # weights are in data['wmin'] and params are in data['x']
        H = OO._H(data['wmin'][0],data['x'])
        ds = {'chi2_exact_hessian':H.tolist(), 'test':args.INDIR,'result':file,'pnames':data['pnames']}
        outfile = os.path.join(args.OUTDIR, "{}_{}_exact_hessian.json".format(args.OFILEPREFIX, typefile))
        with open(outfile, 'w') as f:
            json.dump(ds, f, indent=4)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Exact Hessian',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir for the plot file")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix (No spaces)")
    parser.add_argument("-p","--paramfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Parameter files in which output is stored.")
    parser.add_argument("--onlycheck", dest="CHECK", default=False, action="store_true",
                        help="Only check for existence of parameter files")
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored ")

    args = parser.parse_args()
    print(args)
    if args.CHECK: checkexists(args)
    else:
        if args.INDIR is None:
            print("Indir required for getting exact hessian. Please provide one using -i or --indir option")
            sys.exit(1)
        checkexists(args)
        buildexacthessian(args)
