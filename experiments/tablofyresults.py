import numpy as np
import apprentice
import os, sys

def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def checkTypes(args):
    import re
    typearrlab = []
    typearr = []
    typecleanarr = []
    typename = []
    hparam = []
    for fnum,r in enumerate(args.FILES):
        if r == "d":
            typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('True P')
            typename.append('d')
            hparam.append(-1)
        elif "gradO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            typename.append('FIM')
            hparam.append(mu)
            if mu == 0:
                typearr.append('\\multicolumn{1}{|c|}{FIM \\eqref{eq:bilevel_FIM}}')
                typearrlab.append('FIM')
                typecleanarr.append('FIM without bound')
            else:
                typearr.append("\\multicolumn{1}{|c|}{\makecell{FIM w/\\\\bound\\eqref{eq:bilevel_FIM_withbound}\\\\$\\mu=%d$}}"%(mu))
                typearrlab.append('FIM w/ b ($\{}={}$)'.format(args.REGPARAMNAME[fnum],mu))
                typecleanarr.append('FIM with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum],mu))
        elif "robO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            typename.append('robustOpt')
            hparam.append(mu)
            typearr.append("\multicolumn{1}{|c|}{\makecell{Robust\\\\Opt\\eqref{eq:robo_form_final}\\\\$\\mu=%d$}}"%(mu))
            typearrlab.append('RobO w/ b ($\{}={}$)'.format(args.REGPARAMNAME[fnum],mu))
            typecleanarr.append('Robust Opt with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum], mu))
        else:
            findex = -1
            otherapproaches = ['portfolio', 'medscore', 'meanscore', # Juli/Wenjing's approaches
                               'CTEQ', 'MSTW', 'NNPDF', 'HERA'] # Approaches in A14 paper
            for othernum,other in enumerate(otherapproaches):
                if other in r:
                    findex= othernum
                    break
            if findex == -1: raise Exception("Unknown approach {}".format(r))
            typename.append(otherapproaches[findex])
            hparam.append(-1)
            typearr.append(
                "\multicolumn{1}{|c|}{%s}" % (otherapproaches[findex]))
            typearrlab.append('{}'.format(otherapproaches[findex]))
            typecleanarr.append('{}'.format(otherapproaches[findex]))


    # p0 = [2.47,1.71]
    # p1 = [3.0,1.2]
    # if "4gw2p_1wp0_1wp1_1wp0_1wp1" in indir:
    #     parr = [p0,p1,p0,p1]
    #     parrstr = "$p_0~p_1~p_0~p_1$"
    # elif '4gw2p_1wp0_1wp1_2wp0' in indir:
    #     parr = [p0, p1, p0, p0]
    #     parrstr = "$p_0~p_1~p_0~p_0$"
    # elif '4gw2p_2wp0_2wp1' in indir:
    #     parr = [p0, p0, p1, p1]
    #     parrstr = "$p_0~p_0~p_1~p_1$"
    # elif '4gw2p_3wp0_1wp1' in indir:
    #     parr = [p0, p0, p0, p1]
    #     parrstr = "$p_0~p_0~p_0~p_1$"
    # else:
    #     parr = [p0,p0,p0,p0]
    #     parrstr = "$p_0~p_0~p_0~p_0$"

    return typearr,typearrlab, typecleanarr, typename, hparam

def checkexists(args):
    k = 0
    resultdir = args.RESDIR
    for t in args.FILES:
        if t == 'd': continue
        e = os.path.exists(os.path.join(resultdir,t))
        print(e, os.path.join(resultdir,t))
        if not e: k = 1
    if k: sys.exit(1)

def resolveParam(paramarr, dim, hnames):
    pdict = {}
    print(hnames)
    pstr = ""
    if len(paramarr) == dim:
        param  = [float(p) for p in paramarr]
        for ono,obs in enumerate(hnames):
            pdict[obs] = param
            pstr += "$p_0$"
            if ono != len (hnames):
                pstr += "~"
    elif len(paramarr) == (len(hnames) * dim):
        index = 0
        subindex = 0
        for ono,obs in enumerate(hnames):
            param = [float(paramarr[index+d]) for d in range(dim)]
            index += dim
            if ono ==0:
                pstr += "$p_{}$".format(subindex)
            else:
                subindex+=1 if param in pdict.values() else 0
                pstr += "$p_{}$".format(subindex)
            if ono != len(hnames):
                pstr += "~"
            pdict[obs] = param

    else: raise Exception("Number of parameters should equal to "
                          "number of params or number of params * number of observables "
                          "and not {}".format(len(paramarr)))
    print(pdict)
    return pdict,pstr

def calcchi2(y, d,e):
    if e == 0:
        return (y-d) ** 2
    else:
        return (y-d) ** 2 / (e ** 2)

def tablofyresults(indir,approxfile,expdatafile,args,typearr, typecleanarr, typename, hparam):
    checkexists(args)
    if approxfile is None:
        print("No approximation file specified")
        sys.exit(1)

    if expdatafile is None:
        print("No data file specified")
        sys.exit(1)
    binids, RA = apprentice.tools.readApprox(approxfile)
    # keepidfile = os.path.join(indir,"keepids.json")
    # if not os.path.exists(keepidfile):
    #     print("keepid file required. Run {} with --onlyfilter and -i options first".format(sys.argv[0]))
    #     sys.exit(1)
    # import json
    # with open(indir+"/keepids.json", 'r') as f:
    #     keepids = json.load(f)
    # keepidindexes = [binids.index(x) for x in keepids]
    # binids = list(np.array(binids)[keepidindexes])
    # RA = list(np.array(RA)[keepidindexes])

    dim = RA[0].dim
    dd = apprentice.tools.readExpData(expdatafile, binids)
    hnames = sorted(list(set([b.split("#")[0] for b in binids])))

    if 'd' in args.FILES:
        pdict,parrstr = resolveParam(args.PARAM,dim,hnames)

    s = ""
    chi2data={}
    for tno,type in enumerate(args.FILES):
        chi2data[type] = []
        s+=typearr[tno]
        for hno, hn in enumerate(hnames):
            _usedbins = [b for b in binids if hn in b]
            i_used = [binids.index(b) for b in _usedbins]
            D = [dd[b][0] for b in _usedbins]
            DY = [dd[b][1] for b in _usedbins]
            if type == 'd':
                Y = [RA[i].predict(pdict[hn]) for i in i_used]
                chi2arr = [calcchi2(Y[i],D[i],DY[i]) for i in range(len(_usedbins))]
                chi2 = sum(chi2arr)
                chi2data[type].append(chi2)
                if hno ==0:
                    s+='&\\multicolumn{2}{|c|}{%s}'%(parrstr)
                s += '&%s&%.2f' % ('--',chi2/len(_usedbins))
            else:
                data = readReport(os.path.join(args.RESDIR,type))
                Y = [RA[i].predict(data["x"]) for i in i_used]
                chi2arr = [calcchi2(Y[i],D[i],DY[i]) for i in range(len(_usedbins))]
                chi2 = sum(chi2arr)
                chi2data[type].append(chi2)
                if hno ==0:
                    s+='&\multicolumn{1}{|c|}{%.4f}\n' \
                       '&\multicolumn{1}{|c|}{%.4f}'%(data['x'][0],data['x'][1])
                # s += '&%.1E&%.2f' % (abs(data['wmin'][0][hno]), chi2/len(_usedbins))
                s += '&bug&%.2f' % (chi2/len(_usedbins))
        s+="\\\\\\hline\n"


    print("\n\n"+s)
    print("\n\n###########################\n\n")
    cepsdata ={}
    for tno1, type1 in enumerate(args.FILES):
        for tno2, type2 in enumerate(args.FILES):
            if tno1 <= tno2: continue
            chi2data1 = chi2data[type1]
            chi2data2 = chi2data[type2]
            c1count = 0
            c2count = 0
            c1better = []
            c2better = []
            index = 1
            for c1,c2 in zip(chi2data1,chi2data2):
                if c1>c2:
                    c2count+=1
                    c2better.append(index)
                elif c2>c1:
                    c1count += 1
                    c1better.append(index)
                index+=1
            # print(len(chi2data1))
            # print(repr(c1better))
            # print(repr(c2better))
            cepsdata[type1 + "_" + type2] = c1count
            cepsdata[type2 + "_" + type1] = c2count
            print("{}\t\t{}\t\t{}\t\t{}".format(os.path.basename(type1),os.path.basename(type2),c1count,c2count))
    print("\n\n###########################\n\n")

    import math
    print("halfway point is {}".format(math.ceil(len(hnames) / 2)))
    for tno1, type1 in enumerate(args.FILES):
        if(tno1 < len(args.FILES)-1):
            print("{}\t\t{}".format(cepsdata[type1 + "_" + args.FILES[-1]],os.path.basename(type1)))

    print("\n\n###########################\n\n")

    print("\n\n" + s)
    cepstable = "\\begin{tabular}{c"
    for tno1, type1 in enumerate(args.FILES):
        cepstable += "|c"
    cepstable += "|}\n\cline{2-%s}\n" % (len(args.FILES) + 1)
    for tno1, type1 in enumerate(args.FILES):
        cepstable += " & {}".format(tno1 + 1)
    cepstable += "\\\\\n\cline{1-%s}\n" % (len(args.FILES) + 1)
    for tno1, type1 in enumerate(args.FILES): # ROW
        cepstable += "\multicolumn{1}{|c|}{%d}"%(tno1+1)
        for tno2, type2 in enumerate(args.FILES): # COLUMN
            if tno1 == tno2:
                cepstable += "& --"
                continue
            val = cepsdata[type1 + "_" + type2]
            color = 'orange'
            half = math.ceil(len(hnames) / 2)
            if len(hnames) % 2 ==1:
                if val<half:
                    color = 'red'
                elif val>=half:
                    color = 'green'
            else:
                if val<half:
                    color = 'red'
                elif val>half:
                    color = 'green'
            cepstable += "& \cellcolor{%s!35} %d"%(color,val)
        cepstable += " \\\\\\hline\n"
    cepstable += "\\end{tabular}"

    print("\n\n"+cepstable+"\n\n")

    cepslegend = "\\begin{enumerate}\n"
    for tno, type in enumerate(args.FILES):
        cepslegend += "\t\item {}\n".format(typecleanarr[tno])
    cepslegend += "\\end{enumerate}"

    print("\n\n" + cepslegend + "\n\n")
    pstr = ""
    paramcomp = "\\begin{tabular}{|c"
    for tno1, type1 in enumerate(args.FILES):
        paramcomp += "|c"
    paramcomp += "|}\n\cline{1-%s}\n" % (len(args.FILES) + 1)
    paramcomp += "pname"
    for tno1, type1 in enumerate(args.FILES):
        paramcomp += "&%d "%(tno1+1)
    paramcomp += "\\\\\\cline{1-%s}\n" % (len(args.FILES) + 1)
    data = readReport(os.path.join(args.RESDIR, args.FILES[-1]))
    pnames = data['pnames']
    for pnum,param in enumerate(pnames):
        paramcomp += '%s'%(param)
        pstr += '%s\t'%(param)
        for tno1, type1 in enumerate(args.FILES):
            if type1 == 'd':
                paramcomp += '&-'
            else:
                data = readReport(os.path.join(args.RESDIR, type1))
                paramcomp += "&%.2f"%(data['x'][pnum])
                pstr += "%.4f "%(data['x'][pnum])
        pstr+='\n'
        paramcomp += " \\\\\\hline\n"
    paramcomp += "\\end{tabular}"

    print("\n\n" + paramcomp + "\n\n")
    print("\n\n" + pstr + "\n\n")

    # plot change in C_eps w.r.t. mu
    for cno, choice in enumerate(['FIM', 'robustOpt']):
        X = []
        Y = []
        for tno, type in enumerate(args.FILES):
            if tno == len(args.FILES) - 1: continue
            if typename[tno] == choice:
                X.append(hparam[tno])
                val = cepsdata[type + "_" + args.FILES[-1]]
                Y.append(val)
        if len(X) >0:
            # print(np.c_[X,Y])
            import pylab
            params = {'legend.fontsize': 10,
                      'legend.handlelength': 2}
            pylab.rcParams.update(params)
            pylab.clf()
            pylab.yscale('log')
            pylab.xlabel("hp")
            pylab.ylabel("$C_\epsilon(p_{\\mathrm{%s\\ with\\ hp}},p_{%s})$"%(choice,typename[-1]))
            pylab.step(X, Y, where='post', marker="", linewidth=0.3, linestyle="-", color = 'red')
            # pylab.show()
            pylab.savefig(os.path.join(args.OUTDIR, "{}_{}_wrt_mu.pdf".format(args.OFILEPREFIX, choice)))


    # for f in args.FILES:
    #     if f == 'd'
    # for pno1, param in enumerate():
    #     cepstable += "|c"



def plotresults(approxfile,expdatafile,args,typearr,typearrlab):
    checkexists(args)
    if approxfile is None:
        print("No approximation file specified")
        sys.exit(1)

    if expdatafile is None:
        print("No data file specified")
        sys.exit(1)

    COL = ["r", "b", "g", "m", "c", "y","k"]
    binids, RA = apprentice.tools.readApprox(approxfile)
    dd = apprentice.tools.readExpData(expdatafile, binids)
    hnames = sorted(list(set([b.split("#")[0] for b in binids])))

    dim = RA[0].dim

    if 'd' in args.FILES:
        pdict, parrstr = resolveParam(args.PARAM, dim, hnames)

    import pylab
    params = {'legend.fontsize': 10,
              'legend.handlelength': 2}
    pylab.rcParams.update(params)
    for hno, hn in enumerate(hnames):
        _usedbins = [b for b in binids if hn in b]
        _usedbins.sort(key=lambda x: int(x.split("#")[1]))
        X = [int(x.split("#")[1]) for x in _usedbins]
        i_used = [binids.index(b) for b in _usedbins]
        D = [dd[b][0] for b in _usedbins]
        DY = [dd[b][1] for b in _usedbins]
        pylab.clf()
        pylab.xlabel("bins")
        pylab.ylabel("$r_b(\\hat{p}_w)\\ or\\ \\mathcal{R}_b$")
        # pylab.title(expdatafile.split('/')[-2] + "\n" + hn)
        pylab.errorbar(X, D, DY, color="k", marker="o",markersize=5, linewidth=0,  label="Data",drawstyle='steps-mid')
        # rmax = 0.35
        # rmin = -0.1
        # rmax = 14
        # rmin = -0.5
        # rmin = -0.02
        # print(max(Y_all), min (Y_all))
        # if ((max(D) < rmax) and (min(D) > rmin)):
        #     pylab.ylim(rmin, rmax)
        pylab.yscale('log')
        # pylab.ylim(10**-10,14)

        wt = ""
        marker = ["","*","o"]
        lw = [1.5,1.5,1]
        labelarr = ['Const Wts','FIM with $\mu=60$','FIM with $\mu=200$']
        for tno,type in enumerate(args.FILES):
            if type == 'd':
                Y = [RA[i].predict(pdict[hn]) for i in i_used]
                chi2 = sum([calcchi2(Y[i],D[i],DY[i]) for i in range(len(_usedbins))])
            else:
                data = readReport(os.path.join(args.RESDIR,type))
                Y = [RA[i].predict(data["x"]) for i in i_used]
                chi2 = sum([calcchi2(Y[i],D[i],DY[i]) for i in range(len(_usedbins))])
                wt = "($w_\mathcal{O}$=%.1E)"%data['wmin'][0][hno]
            gof = "$\chi^2/ndf={:.2f}$".format(chi2 / len(_usedbins))
            pylab.step(X, Y, where='mid',color=COL[tno], marker="", linewidth=0.3, linestyle="-", label="{} {} {}".format(typearrlab[tno], gof,wt))
            # pylab.step(X, Y, where='mid', color=COL[tno], marker="", linewidth=lw[tno], linestyle="-",
            #            label="{} {}".format(labelarr[tno], wt))

        pylab.legend()
        if not os.path.exists(args.OUTDIR):
            os.makedirs(args.OUTDIR,exist_ok=True)
        pylab.savefig(os.path.join(args.OUTDIR,"{}_{}.pdf".format(args.OFILEPREFIX, hn.replace("/", "_"))))

def filterandstorekeepbinids(approxfile,expdatafile,weightfile,outdir):
    from apprentice.tools import TuningObjective
    IO  = TuningObjective(weightfile,expdatafile,approxfile)
    import json
    with open(outdir+"/keepids.json",'w') as f:
        json.dump(obj=IO._binids,fp=f)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Tablofy and plot',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir")
    parser.add_argument("-r","--resultdir", dest="RESDIR", type=str, default="./",
                        help="Result Dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix")
    parser.add_argument("-t","--resultfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Result files in which output is stored and/or d (for true parameter)")
    parser.add_argument("--onlycheck", dest="CHECK", default=False, action="store_true",
                        help="Only check for existence of runs")
    parser.add_argument("--onlytable", dest="TABLE", default=False, action="store_true",
                        help="Only Table. Don't plot")
    parser.add_argument("--onlyfilter", dest="FILTER", default=False, action="store_true",
                        help="Do Filter and store in input directory for future use")
    parser.add_argument("-p", "--param", dest="PARAM", type=str, default=[], nargs='+',
                        help="Parameter to be used. If len(.) == dim(P), same parameter"
                             " will be used for all observable")
    parser.add_argument("-l", "--regparamname", dest="REGPARAMNAME", default=[], nargs='+',
                        help="Regularization param name per file to find in filenames")
    args = parser.parse_args()

    print(args)
    if len(args.REGPARAMNAME) == 0:
        args.REGPARAMNAME = ['mu'] * len(args.FILES)


    approxfile = args.INDIR+"/approximation.json"
    expdatafile = args.INDIR+"/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.INDIR + "/experimental_data.json"
    if args.FILTER:
        weightfile = args.INDIR + "/weights"
        filterandstorekeepbinids(approxfile,expdatafile,weightfile,args.INDIR)
        sys.exit(0)
    if args.CHECK: checkexists(args)
    else:
        typearr, typearrlab, typecleanarr, typename, hparam = checkTypes(args)
        tablofyresults(args.INDIR,approxfile,expdatafile,args,typearr,typecleanarr, typename, hparam)
        if not args.TABLE:
            plotresults(approxfile,expdatafile,args,typearr,typearrlab)



