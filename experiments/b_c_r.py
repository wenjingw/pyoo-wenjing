#!/usr/env/python
import os,sys,json
import numpy as np
import apprentice

def mymakedir(directory):
    if not os.path.exists(directory): os.makedirs(directory)


def checkfileexists(file):
    if not os.path.exists(file):
        raise Exception('File {} not found'.format(file))


def getfilePaths(outdir,type):
    paths = {}
    d = outdir.split('/')[-1]
    paths['data'] = os.path.join(outdir, type, "data")
    paths['appnest'] = os.path.join(outdir, type, "appnest")
    paths['plot'] = os.path.join(outdir, type, "plots")




    for k in paths.keys():
        mymakedir(paths[k])
    return paths

def resolveParam(paramarr, dim, hnames,verbose):
    pdict = {}
    if verbose: print(hnames)
    if len(paramarr) == dim:
        param  = [float(p) for p in paramarr]
        for obs in hnames:
            pdict[obs] = param
    elif len(paramarr) == (len(hnames) * dim):
        index = 0
        for obs in hnames:
            param = [float(paramarr[index+d]) for d in range(dim)]
            index+=dim
            pdict[obs] = param
    else: raise Exception("Number of parameters should equal to "
                          "number of params or number of params * number of observables "
                          "and not {}".format(len(paramarr)))
    if verbose: print(pdict)
    return pdict

def runappnest(apprxfile, datafile, weightfile, outdir, verbose):
    cmd = "mpirun -np 4 python ../../apprentice/bin/app-nest -w {w} " \
          "-d {d} -o {o} {a}".format(w=weightfile,d=datafile,a=apprxfile,o=outdir)
    cmd+= " -v" if verbose else ""
    if verbose: print(cmd)
    return os.system(cmd)

def buildcontourplot(apprxfile, datafile, weightfile, outfile, verbose):
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm
    IO = apprentice.tools.TuningObjective(weightfile, datafile,apprxfile)
    PMIN = IO._SCLR.box[:, 0]
    PMAX = IO._SCLR.box[:, 1]
    PNAMES = IO.pnames

    if len(PMIN) > 2:
        raise Exception("Cannot handle dimention not equal to 2")
    X = np.linspace(PMIN[0],PMAX[0],100)
    Y = np.linspace(PMIN[1], PMAX[1],100)
    X,Y = np.meshgrid(X,Y)
    nr, nc = np.shape(X)
    Z = np.zeros((nr,nc), dtype=np.float)

    for i in range(nr):
        for j in range(nc):
            Z[i][j] = IO([X[i][j],Y[i][j]])

    levels = [-1.0]
    amin = np.amin(Z)
    amax = np.amax(Z)
    if verbose: print("Z min and max are:",amin,amax)
    rng = np.linspace(0,amax,100)
    for r in rng: levels.append(r)
    cp = plt.contour(X, Y, Z, levels, colors='black', linestyles='dashed', linewidths=0.8)
    plt.clabel(cp, inline=1, fontsize=8)
    plt.xlabel(PNAMES[0])
    plt.ylabel(PNAMES[1])
    plt.savefig(outfile)
    plt.clf()
    plt.close()


def do(args):
    if args.DEBUG: print(args)
    weight = [float(w) for w in args.WEIGHT]
    noise = [float(n) for n in args.NOISE]
    if len(noise) == 0 or sum(noise) == 0:
        raise Exception("Noise has to be non zero. Strange things happen with zero noise. Noise is: {}".format(repr(noise)))
    fp = getfilePaths(args.OUTDIR,args.TYPE)
    binids, RA = apprentice.tools.readApprox(args.APPROX)

    with open(args.APPROX,'r') as f:
         approxall = json.load(f)
    f.close()

    dim = RA[0].dim
    hnames = sorted(list(set([b.split("#")[0] for b in binids])))
    pdict = resolveParam(paramarr=args.PARAM,dim=dim,hnames=hnames,verbose=args.DEBUG)

    bias = [float(b) for b in args.BIAS]
    if len(bias) == 0:
        bias = [0. for h in hnames]
    dataall = {}
    # dataall_ro = {}
    for obno,obs in enumerate(hnames):
        data = {}
        # data_ro = {}
        approx = {}
        param = pdict[obs]
        for bno, bin in enumerate(binids):
            if obs in bin:
                d = RA[bno](param)
                d += np.random.normal(0, noise[obno])
                d += bias[obno]
                if args.PEROBS: data[bin] = [d, noise[obno]]
                # if args.PEROBS: data_ro[bin] = [d, 0.0005]
                if args.PEROBS: approx[bin] = approxall[bin]
                dataall[bin] = [d, noise[obno]]
                # dataall_ro[bin] = [d, 0.0005]

            if args.PEROBS:
                weightstr = "{}   1.0\n".format(obs) if len(weight) ==0 else "{}   {}\n".format(obs,weight[obno])

                with open(os.path.join(fp['data'], "experimental_data_obs{}.json".format(obno+1)), 'w') as f:
                    json.dump(data, f, indent=4)
                f.close()

                # with open(os.path.join(fp['data'], "experimental_data_ro_obs{}.json".format(obno+1)), 'w') as f:
                #     json.dump(data_ro, f, indent=4)
                # f.close()

                with open(os.path.join(fp['data'], "approximation_obs{}.json".format(obno+1)), 'w') as f:
                    json.dump(approx, f, indent=4)
                f.close()

                with open(os.path.join(fp['data'], 'weights_obs{}.txt'.format(obno+1)), 'w') as f:
                    f.write(weightstr)
                f.close()

    weightstr = ""
    for obno,obs in enumerate(hnames):
        weightstr += "{}   1.0\n".format(obs) if len(weight) ==0 else "{}   {}\n".format(obs,weight[obno])

    with open(os.path.join(fp['data'],"experimental_data.json"),'w') as f:
        json.dump(dataall,f,indent=4)
    f.close()

    # with open(os.path.join(fp['data'],"experimental_data_ro.json"),'w') as f:
    #     json.dump(dataall_ro,f,indent=4)
    # f.close()

    with open(os.path.join(fp['data'],"approximation.json"),'w') as f:
        json.dump(approxall,f,indent=4)
    f.close()

    with open(os.path.join(fp['data'],'weights'),'w') as f:
        f.write(weightstr)
    f.close()

    for obno,obs in enumerate(hnames):
        if args.PEROBS:
            datafile = os.path.join(fp['data'], "experimental_data_obs{}.json".format(obno+1))
            weightfile = os.path.join(fp['data'], 'weights_obs{}.txt'.format(obno+1))
            approxfile = os.path.join(fp['data'], "approximation_obs{}.json".format(obno+1))
            outdir = os.path.join(fp['appnest'], 'appnest_obs{}'.format(obno+1))
            if args.MULTINEST:
                runappnest(apprxfile=approxfile, datafile=datafile,weightfile=weightfile,outdir=outdir,verbose=args.DEBUG)
            outfile = os.path.join(fp['plot'], 'contour_obs{}'.format(obno+1))
            if args.CONTOUR:
                buildcontourplot(apprxfile=approxfile, datafile=datafile,
                                              weightfile=weightfile,outfile=outfile,verbose=args.DEBUG)

    datafile = os.path.join(fp['data'], "experimental_data.json")
    weightfile = os.path.join(fp['data'], 'weights')
    outdir = os.path.join(fp['appnest'], 'appnest_all')
    if args.MULTINEST:
        runappnest(apprxfile=args.APPROX, datafile=datafile, weightfile=weightfile, outdir=outdir,verbose=args.DEBUG)
    outfile = os.path.join(fp['plot'], 'contour_all')
    if args.CONTOUR:
        buildcontourplot(apprxfile=args.APPROX, datafile=datafile,
                                      weightfile=weightfile, outfile=outfile, verbose=args.DEBUG)


    # plotresults(approxfile=args.APPROX, expdatafile=datafile, typearr=[None],
    #             pdict=pdict, labelarr=['Data'], outdir=fp['plot'],
    #             outfileprefix=args.TYPE)
    if args.HIST:
        pstr = " ".join(args.PARAM)

        plotcmd = " python tablofyresults.py -i {i} -r \"\" -o {o} -t d -p {p} -x {x}". \
            format(i=fp['data'], o=fp['plot'], p=pstr, x=args.TYPE)
        if args.DEBUG: print(plotcmd)
        os.system(plotcmd)





# python b_c_r.py -t b -a results/b_c_r/sip33_3gw1bwoutbias2p_constnoise/approximation.json -o results/b_c_r/sip33_3gw1bwoutbias2p_constnoise -p 2.47 1.71 -m -s -n 0.0005 0.0005 0.0005 0.0005 -b 0 0 0 0.48394232652795577


#############################################
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Begin and corrupt data',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-t", "--type", dest="TYPE", type=str, default='b',
                  help="Type of run: b for beginning, c for corrupt")

    parser.add_argument("-n","--noise", dest="NOISE", type=str, default=[], nargs='+',
                  help="Noise for each observable")
    parser.add_argument("-b", "--bias", dest="BIAS", type=str, default=[], nargs='+',
                        help="Bias for each observable")

    # TODO Currently only one approx file supportted. If multiple use [] and nargs='+',
    parser.add_argument("-a", "--approx", dest="APPROX", type=str, default=None,
                  help="The path for approximation file for type")
    parser.add_argument("-o", "--out", dest="OUTDIR", type=str, default=None,
                  help="The output directory (for type)")
    parser.add_argument("-p", "--param", dest="PARAM", type=str, default=[], nargs='+',
                  help="Parameter to be used. If len(.) == dim(P), same parameter"
                       " will be used for all observable")
    parser.add_argument("-w", "--weight", dest="WEIGHT", type=str, default=[], nargs='+',
                        help="Weight to be used for each observable")

    parser.add_argument("-v", "--debug", dest="DEBUG", action="store_true", default=False,
                        help="Turn on some debug messages")
    parser.add_argument("-c", "--runcontour", dest="CONTOUR", action="store_true", default=False,
                        help="Set this to generate contour plots per observable and one everything")
    parser.add_argument("-m", "--runmultinest", dest="MULTINEST", action="store_true", default=False,
                        help="Set this to run multinest per observable (if -u or --perobs is set) and on all"
                             "")
    parser.add_argument("-u", "--perobs", dest="PEROBS", action="store_true", default=False,
                        help="Set this to do analysis per observable")
    parser.add_argument("-s", "--plothistogram", dest="HIST", action="store_true", default=False,
                        help="Set this to plot histograms")


    args = parser.parse_args()

    do(args)



