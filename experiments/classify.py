# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
plt.style.use("ggplot")
import pandas as pd
import json
#with open(sys.argv[1]) as f:
#    D = json.load(f)
with open('singletune_clean_a14_la_30_fitrange.json') as f:
    D = json.load(f)
V = [v[0] for k, v in D.items()]
name = [k for k, v in D.items()]
pname = ['SigmaProcess:alphaSvalue',
'BeamRemnants:primordialKThard',
'SpaceShower:pT0Ref',
'SpaceShower:pTmaxFudge',
'SpaceShower:pTdampFudge',
'SpaceShower:alphaSvalue',
'TimeShower:alphaSvalue',
'MultipartonInteractions:pT0Ref',
'MultipartonInteractions:alphaSvalue',
'BeamRemnants:reconnectRange']
pshortname = ['p0','p1','p2','p3',
'p4','p5','p6','p7','p8','p9']
parameters = [v[1:11] for k, v in D.items()] # parameter valus of 406 obs
#plt.scatter(range(50),first50)

pstar = pd.DataFrame(parameters,index =name) 
pstar.columns = pname
pstar.columns = pshortname
pstar_50 = pstar[0:50] # parameter values of the first 50 observables

export_csv = pstar.to_csv (r'pstar.csv', header=True)
#import seaborn as sns
#sns.set(rc={'axes.facecolor' : '#EEEEEE'})
#sns.boxplot(pstar_50)         

pstar_normalized = pd.read_csv('pstar_normalized.csv',index_col='name')       
pstar_normalized_50 = pstar_normalized[0:50] # parameter values of the first 50 observables

plt.figure(figsize=(25,20))
plt.boxplot(pstar_normalized_50.T)
#plt.title('parameter values')
plt.title('charged jet multiplicity')
plt.ylabel('parameter values)');
#plt.xticks(np.arange(12),pshortname);
plt.savefig("p_boxplot_50.pdf")


from sklearn.cluster import KMeans
# Declaring Model
model = KMeans(n_clusters=10)

# Fitting Model
model.fit(pstar_normalized)

# Prediction on the entire data
all_predictions = model.predict(pstar_normalized)

# Printing Predictions
print(all_predictions)

# https://blog.cambridgespark.com/how-to-determine-the-optimal-number-of-clusters-for-k-means-clustering-14f27070048f
Sum_of_squared_distances = []
K = range(1,50)
for k in K:
    km = KMeans(n_clusters=k)
    km = km.fit(pstar_normalized)
    Sum_of_squared_distances.append(km.inertia_)
    
#plt.figure(figsize=(25,20))
plt.plot(K, Sum_of_squared_distances, 'bx-')
plt.xlabel('k')
plt.ylabel('Sum_of_squared_distances')
plt.title('Elbow Method For Optimal k')
plt.savefig("p_elbow.pdf")

# 10

k_means = KMeans(n_clusters=10, max_iter=50, random_state=1)
k_means.fit(pstar_normalized) 
labels = k_means.labels_
clusterid = pd.DataFrame(labels, index=name, columns=['Cluster ID'])


centroids = k_means.cluster_centers_
pd.DataFrame(centroids,columns=pstar_normalized.columns)

n_clusters_ = len(np.unique(labels))

df = pstar_normalized
plt.scatter(df['p0'], df['p1'], c= k_means.labels_.astype(float), s=50, alpha=0.5)
plt.scatter(centroids[:, 0], centroids[:, 1], c='black', s=50)
plt.xlabel('p0')
plt.ylabel('p1')
plt.savefig("p_p0p1.pdf")

export_csv = clusterid.to_csv (r'pstar_cluster.csv', header=True)




#####


pstar_normalized_T = pstar_normalized.T
Sum_of_squared_distances = []
K = range(1,10)
for k in K:
    km = KMeans(n_clusters=k)
    km = km.fit(pstar_normalized_T)
    Sum_of_squared_distances.append(km.inertia_)
    
#plt.figure(figsize=(25,20))
plt.plot(K, Sum_of_squared_distances, 'bx-')
plt.xlabel('k')
plt.ylabel('Sum_of_squared_distances')
plt.title('Elbow Method For Optimal k')
plt.savefig("p_elbow_p.pdf")

# 4

k_means = KMeans(n_clusters=4, max_iter=50, random_state=1)
k_means.fit(pstar_normalized_T) 
labels = k_means.labels_
clusterid = pd.DataFrame(labels, index=pname, columns=['Cluster ID'])


centroids = k_means.cluster_centers_
pd.DataFrame(centroids,columns=pstar_normalized_T.columns)

n_clusters_ = len(np.unique(labels))

export_csv = clusterid.to_csv (r'pstar_cluster_p.csv', header=True)

